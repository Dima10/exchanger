<?php
namespace ExchangerBundle\CustomModels;
use EntityBundle\Entity\Models\DB;

/**
 * Created by PhpStorm.
 * User: alastar
 * Date: 26.11.18
 * Time: 22:19
 */
class Memcache {

    private function __clone() {}
    private function __construct() {}
    private function __wakeup() {}

    private static $instance = null;
    private static $enabled = false;

    public static function getMemcache()
    {
        if(self::$enabled != true){
            return false;
        }

        try{
            if (null === self::$instance)
            {
                $mc = memcache_connect('localhost', 11211);
                self::$instance = $mc;
            }
            return new self;
        } catch (\Exception $exception){
            return false;
        }

    }

    public function set($id, $data, $time){
        memcache_set(self::$instance, $id, $data, 0, $time);
    }

    public function get($id){
        return memcache_get(self::$instance, $id);
    }
}