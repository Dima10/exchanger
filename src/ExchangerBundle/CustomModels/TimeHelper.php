<?php
namespace ExchangerBundle\CustomModels;
use EntityBundle\Entity\Models\DB;

/**
 * Created by PhpStorm.
 * User: alastar
 * Date: 26.11.18
 * Time: 22:19
 */
class TimeHelper {

    const HALF_HOUR = 1800;
    const HOUR = 3600;
    const TWO_HOURS = 7200;
    const THREE_HOURS = 10800;
    const SIX_HOURS = 21600;
    const TWELVE_HOURS = 43200;
    const DAY = 86400;
    const WEEK = 604800;
    const MONTH = 2419200;

}