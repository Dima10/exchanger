<?php
namespace ExchangerBundle\CustomModels;
use EntityBundle\Entity\Models\DB;

/**
 * Created by PhpStorm.
 * User: alastar
 * Date: 26.11.18
 * Time: 22:19
 */
class SeoMeta {

    private function __clone() {}
    private function __construct() {}
    private function __wakeup() {}

    private static $instance = null;
    protected $seoTitle;
    protected $seoKeywords;
    protected $seoDescription;
    protected $seoH1;

    public static function getSeometa()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return mixed
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @return mixed
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * @return mixed
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * @return mixed
     */
    public function getSeoH1()
    {
        return $this->seoH1;
    }

    public function setData(array $seoData){
        if(isset($seoData['seoTitle']) && !empty($seoData['seoTitle'])){
            $this->seoTitle = $seoData['seoTitle'];
        }

        if(isset($seoData['seoKeywords']) && !empty($seoData['seoKeywords'])){
            $this->seoKeywords = $seoData['seoKeywords'];
        }

        if(isset($seoData['seoDescription']) && !empty($seoData['seoDescription'])){
            $this->seoDescription = $seoData['seoDescription'];
        }

        if(isset($seoData['seoH1']) && !empty($seoData['seoH1'])){
            $this->seoH1 = $seoData['seoH1'];
        }
    }

    public function setDataByUrl($url)
    {

        $memcache = Memcache::getMemcache();
        if($memcache !== false){
            $result = $memcache->get('setSeometaByUrl_'.md5($url));
            if($result != null){
                $this->seoH1 = $result['seo_h1'];
                $this->seoTitle = $result['seo_title'];
                $this->seoKeywords = $result['seo_keywords'];
                $this->seoDescription = $result['seo_description'];
            } else {

                $seo = $queryResult = DB::getConnection()->fetchAssoc('Select * from seo where url = "'.$url.'"');
                $this->seoH1 = $seo['seo_h1'];
                $this->seoTitle = $seo['seo_title'];
                $this->seoKeywords = $seo['seo_keywords'];
                $this->seoDescription = $seo['seo_description'];

                $memcache->set('setSeometaByUrl_'.md5($url),$seo, TimeHelper::DAY);

            }
        } else {
            $seo = $queryResult = DB::getConnection()->fetchAssoc('Select * from seo where url = "'.$url.'"');

            $this->seoH1 = $seo['seo_h1'];
            $this->seoTitle = $seo['seo_title'];
            $this->seoKeywords = $seo['seo_keywords'];
            $this->seoDescription = $seo['seo_description'];

        }
    }

    public function getDataArray(){
        return [
            'seoH1' => $this->seoH1,
            'seoTitle' => $this->seoTitle,
            'seoKeywords' => $this->seoKeywords,
            'seoDescription' => $this->seoDescription,
        ];
    }
}