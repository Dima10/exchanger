<?php

namespace ExchangerBundle\Extensions;

use EntityBundle\Entity\Models\UsersModel;
use ExchangerBundle\CustomModels\SeoMeta;
use Twig_Extension;
use Twig_Extension_GlobalsInterface;

class Globals extends Twig_Extension implements Twig_Extension_GlobalsInterface {

    public function getGlobals() {
        $data = array();

        $data['user'] = UsersModel::getUser();
        $data['seoMeta'] = SeoMeta::getSeometa();

        return $data;
    }

}