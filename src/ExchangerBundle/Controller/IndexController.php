<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Feedback;
use EntityBundle\Entity\Models\GameModel;
use ExchangerBundle\CustomModels\Games;
use ExchangerBundle\Extensions\simple_html_dom;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends BaseController
{
    public function indexAction()
    {
//        $this->setJavascriptOutput('/js/backbone/controllers/BackboneIndexController.js');

        $gamesData = GameModel::getActiveGamesList();

        $gamesList = [];
        foreach ($gamesData as $game){
            if($game['parent'] == 0){
                $gamesList[] = [
                                    'name'=>$game['name'],
                                    'href'=>$game['href'],
                                    'bigLogo'=>$game['biglogo'],
                                ];
            }
        }

        return $this->render('ExchangerBundle:index:index.html.twig',
            array(
                'gamesList' => $gamesList,
                'seoMeta'=>$this->seoMeta->getDataArray(),
                'javascriptOutput'=>$this->javascriptOutput
            )
        );
    }

    public function feedbackAction()
    {
        $this->setJavascriptOutput('/js/backbone/controllers/BackboneFeedbackController.js');
        $this->setJavascriptOutput('/js/validator/validator.js/validator.min.js');

        if(isset($_POST['feedbackSubmit'])){
            if((isset($_POST['name']) && !empty($_POST['name'])) && (isset($_POST['email']) && !empty($_POST['email'])) && (isset($_POST['text']) && !empty($_POST['text']))){
                $feedback = new Feedback();
                $feedback->name = $_POST['name'];
                $feedback->email = $_POST['email'];
                $feedback->text = $_POST['text'];

                $feedback->userId = 0;
                $feedback->date = time();
                $feedback->answered = 0;

                $em = $this->getDoctrine()->getManager();
                $em->persist($feedback);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Обращение успешно зарегистрировано, спасибо');

                return $this->redirectToRoute('exchanger_feedback');
            }
        }

        return $this->render('ExchangerBundle:index:feedback.html.twig', ['javascriptOutput'=>$this->javascriptOutput]);
    }

    public function getDataFromFunPay()
    {
        $base = 'https://funpay.ru/chips/10/';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $base);
        curl_setopt($curl, CURLOPT_REFERER, $base);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($curl);
        curl_close($curl);

        $html = new simple_html_dom();
        $html->load($str);

        $servers = [];
        foreach($html->find('table[class="table table-condensed table-hover table-clickable showcase-table table-sortable"] tbody tr') as $tr){
//        foreach($html->find('div.text-muted') as $tr){
            echo $tr->firstChild()->plaintext.' - '.$tr->lastChild()->plaintext;
            if(isset($servers[$tr->firstChild()->plaintext]['count']) && $servers[$tr->firstChild()->plaintext]['count']>=3){
                continue;
            }
            $servers[$tr->firstChild()->plaintext] = [
                'count'=> isset($servers[$tr->firstChild()->plaintext]['count']) ? $servers[$tr->firstChild()->plaintext]['count']+1 : 1,
                'amount'=> isset($servers[$tr->firstChild()->plaintext]['amount']) ? $servers[$tr->firstChild()->plaintext]['amount']+floatval($tr->lastChild()->plaintext) : floatval($tr->lastChild()->plaintext),
            ];
//            exit;
            echo '<br />';
        }
        echo '<pre>';
        foreach ($servers as $server => $serverInfo){
            echo $server.' - '.round(($serverInfo['amount']/$serverInfo['count']),5);
            echo '<br />';
        }

    }
}