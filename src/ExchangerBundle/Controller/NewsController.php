<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\CurrencyCourse;
use EntityBundle\Entity\Games;
use EntityBundle\Entity\GameSide;
use EntityBundle\Entity\Models\NewsModel;
use EntityBundle\Entity\Models\PaymentSystemsModel;
use EntityBundle\Entity\News;
use EntityBundle\Entity\Servers;
use ExchangerBundle\CustomModels\SeoMeta;

class NewsController extends BaseController
{
    public function indexAction()
    {
        $newsList = NewsModel::getActiveNewsList();
        return $this->render('ExchangerBundle:news:index.html.twig',
            array(
                'newsList' => $newsList,
                'imageDir' => NewsModel::$imagesDir,
            )
        );
    }

    public function newAction(News $new)
    {


        $seoMeta = SeoMeta::getSeometa();
        $seoMeta->setData([
            'seoTitle' => $new->seoTitle,
            'seoKeywords' => $new->seoKeywords,
            'seoDescription' => $new->seoDescription,
        ]);

        return $this->render('ExchangerBundle:news:new.html.twig',
            array(
                'new' => $new,
            )
        );
    }

}