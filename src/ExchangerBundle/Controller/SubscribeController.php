<?php
namespace ExchangerBundle\Controller;

class SubscribeController extends BaseController
{
    public function unsubscribeMailAction()
    {
        return $this->render('ExchangerBundle:subscribe:unsubscribeMail.html.twig');
    }

}