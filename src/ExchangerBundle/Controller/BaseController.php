<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Models\UsersModel;
use ExchangerBundle\CustomModels\SeoMeta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class BaseController extends Controller
{
    public $cssOutput = '';
    public $javascriptOutput = '';
    public $title = '';
    public $description = '';
    public $keywords = '';
    public $seoMeta;
    protected $protocol;
    public $user;

    public function setCSSOutput($cssFilePath, $compile = false){
        $this->cssOutput .= '<link href="'.$cssFilePath.'" type="text/css" rel="stylesheet" media="screen" />'.PHP_EOL;
    }

    public function setJavascriptOutput ($jsFilePath, $compile = false){
        $this->javascriptOutput .= '<script src="'.$jsFilePath.'" type="text/javascript"></script>'.PHP_EOL;
    }

    function __construct()
    {
        $this->protocol = empty($_SERVER['HTTPS'])?'http://':'https://';
        SeoMeta::getSeometa()->setDataByUrl($this->protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
        $this->seoMeta = SeoMeta::getSeometa();

        $this->user = UsersModel::getUser();
    }
}