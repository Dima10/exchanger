<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Helpers\StructureFilesHelper;
use EntityBundle\Entity\Models\DB;
use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\SeoMeta;
use ExchangerBundle\CustomModels\TimeHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class StaticController extends BaseController
{

    public function aboutAction()
    {
        $data = $this->getDataForStatic(4);
        $seoMeta = SeoMeta::getSeometa();
        if(!empty($data['seo']['seoTitle']) && !empty($data['seo']['seoDescription'])){
            $seoMeta->setData($data['seo']);
        }
        return $this->render('ExchangerBundle:static:about.html.twig', $data);
    }

    public function guaranteesAction()
    {
        $data = $this->getDataForStatic(5);
        $seoMeta = SeoMeta::getSeometa();
        if(!empty($data['seo']['seoTitle']) && !empty($data['seo']['seoDescription'])){
            $seoMeta->setData($data['seo']);
        }
        return $this->render('ExchangerBundle:static:guarantees.html.twig', $data);
    }

    public function reviewsAction()
    {
        return $this->render('ExchangerBundle:static:reviews.html.twig');
    }

    public function suppliersAction()
    {
        $data = $this->getDataForStatic(1);
        $seoMeta = SeoMeta::getSeometa();
        if(!empty($data['seo']['seoTitle']) && !empty($data['seo']['seoDescription'])){
            $seoMeta->setData($data['seo']);
        }
        return $this->render('ExchangerBundle:static:suppliers.html.twig',$data);
    }

    public function discountsAction()
    {
        $data = $this->getDataForStatic(2);
        $seoMeta = SeoMeta::getSeometa();
        if(!empty($data['seo']['seoTitle']) && !empty($data['seo']['seoDescription'])){
            $seoMeta->setData($data['seo']);
        }
        return $this->render('ExchangerBundle:static:discounts.html.twig', $data);
    }

    public function coworkingAction()
    {
        return $this->render('ExchangerBundle:static:coworking.html.twig');
    }

    public function contactsAction()
    {
        return $this->render('ExchangerBundle:static:contacts.html.twig');
    }

    public function privacyAction()
    {
        return $this->render('ExchangerBundle:static:privacy.html.twig');
    }

    public function paymentAction()
    {
        $data = $this->getDataForStatic(3);
        $seoMeta = SeoMeta::getSeometa();
        if(!empty($data['seo']['seoTitle']) && !empty($data['seo']['seoDescription'])){
            $seoMeta->setData($data['seo']);
        }
        return $this->render('ExchangerBundle:static:payment.html.twig', $data);
    }

    /**
     * Displays robots.txt.
     */
    public function robotsAction($template = null)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/plain');

        return $this->render($template ?: sprintf(
            "ExchangerBundle:static:robots.txt.twig",
            $this->container->getParameter('kernel.environment')
        ), array(), $response);
    }

    public function sitemapAction($template = null)
    {
        return $this->render(StructureFilesHelper::getFileUploadDirectory().'/sitemap/sitemap.xml', array());
    }

    private function getDataForStatic($id){
        $staticData = [
            'text' => '',
            'seo' => [
                'seoTitle' => '',
                'seoKeywords' => '',
                'seoDescription' => '',
            ],
        ];

        $memcache = Memcache::getMemcache();
        if($memcache !== false){
            $staticData = $memcache->get('staticPage-'.$id);
            if($staticData != null){
                return $staticData;
            } else {
                $queryResult = DB::getConnection()->fetchAssoc('Select * from static_pages where id = '.$id.'');

                $staticData = [
                    'text' => $queryResult['text'],
                    'seo' => [
                        'seoTitle' => $queryResult['seo_title'],
                        'seoKeywords' => $queryResult['seo_keywords'],
                        'seoDescription' => $queryResult['seo_description'],
                    ],
                ];
                $memcache->set('static-'.$id,$staticData, TimeHelper::DAY);

            }
        } else {
            $queryResult = DB::getConnection()->fetchAssoc('Select * from static_pages where id = '.$id.'');

            $staticData = [
                'text' => $queryResult['text'],
                'seo' => [
                    'seoTitle' => $queryResult['seo_title'],
                    'seoKeywords' => $queryResult['seo_keywords'],
                    'seoDescription' => $queryResult['seo_description'],
                ]
            ];

        }

        return $staticData;
    }
}