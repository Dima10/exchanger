<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Models\DB;
use EntityBundle\Entity\Models\GameModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxController extends BaseController
{
    public function getServersByProjectIdAction()
    {

        if(!isset($_POST['projectId']) || empty($_POST['projectId'])){
            return  new JsonResponse(json_encode([]));
        }
        $db = DB::getConnection();
        $result = $db->fetchAll('Select * from servers where game_id ='.$_POST['projectId'].' and active=1');

        return  new JsonResponse(json_encode($result));

    }

}