<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Models\GameModel;
use EntityBundle\Entity\Models\NewsModel;
use ExchangerBundle\CustomModels\Games;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class WidgetController extends Controller
{
    public function gamesListAction($gamePageId = 0)
    {

        $gamesData = GameModel::getActiveGamesList();

        $gamesList = [];
        foreach ($gamesData as $game){
            if($game['parent'] == 0){
                $gamesList[$game['id']] =   [
                    'id'=>$game['id'],
                    'name'=>$game['name'],
                    'logo'=>$game['logo'],
                    'bigLogo'=>$game['biglogo'],
                    'href'=>$game['href'],
                    'internalCategories'=>[

                    ]
                ];
            }
        }

        foreach ($gamesData as $game){
            if($game['parent'] != 0 && isset($gamesData[$game['parent']]) && $gamesData[$game['parent']]['active'] == 1){
                $gamesList[$game['parent']]['internalCategories'][] = [
                    'name'=>$game['name'],
                    'logo'=>$game['logo'],
                    'bigLogo'=>$game['biglogo'],
                    'href'=>$game['href'],
                ];
            }
        }

        unset($gamesList[0]);
        
        return $this->render('ExchangerBundle:widget:gamesList.html.twig', array('gamesList' => $gamesList, 'gamePageId' => $gamePageId));
    }

    public function lastNewsAction($limit)
    {
        return $this->render('ExchangerBundle:widget:lastNews.html.twig', array('news' => NewsModel::getActiveNewsList($limit)));
    }

}