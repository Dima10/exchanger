<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Constants;
use EntityBundle\Entity\CurrencyCourse;
use EntityBundle\Entity\Games;
use EntityBundle\Entity\GameSide;
use EntityBundle\Entity\Models\OrdersModel;
use EntityBundle\Entity\Models\PaymentSystemsModel;
use EntityBundle\Entity\Models\TelegramModel;
use EntityBundle\Entity\Orders;
use EntityBundle\Entity\Servers;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\HttpFoundation\Request;

class ExchangeController extends BaseController
{
    public function indexAction()
    {
//        $this->setJavascriptOutput('/js/backbone/controllers/BackboneGamePageController.js');
//        $this->setJavascriptOutput('/js/validator/validator.min.js');
//        $this->setJavascriptOutput('/js/backbone/components/jquery.mask.js');



        return $this->render('ExchangerBundle:exchange:index.html.twig',
            array(

            )
        );
    }
}