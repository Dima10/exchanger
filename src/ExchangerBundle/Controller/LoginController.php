<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use Doctrine\ORM\EntityRepository;
use EntityBundle\Entity\Constants;
use EntityBundle\Entity\Feedback;
use EntityBundle\Entity\Models\DB;
use EntityBundle\Entity\Models\GameModel;
use EntityBundle\Entity\Models\UsersModel;
use EntityBundle\Entity\Users;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\HttpFoundation\JsonResponse;

class LoginController extends BaseController
{

    public function loginAction()
    {

        $db = DB::getConnection();
        $issetUser = $db->fetchAssoc('SELECT id from users where (email="'.$_POST['nicknameLogin'].'" OR name="'.$_POST['nicknameLogin'].'") AND password = "'.md5($_POST['passwordLogin']).'"');

        if($issetUser != null){
            $user = $this->getDoctrine()
                ->getRepository(Users::class)->find($issetUser['id']);
        } else {
            $user = null;
        }

        if($user != null ){
            $time = time()+(3600*5);
            if(isset($_POST['remember']) && $_POST['remember'] == 'true'){
                $time = time()+(3600*24*360);
            }
            setcookie ("user", serialize($user), $time);
            setcookie ("userName", $user->name, $time);
            return  new JsonResponse(json_encode(['success'=>1]));
        } else {
            return  new JsonResponse(json_encode(['error'=>1]));
        }

    }

    public function logoutAction()
    {
        setcookie ("user", false, time()-500);
        setcookie ("userName", false, time()-500);
        return $this->redirect('/cabinet');
    }

    public function registerAction()
    {

            $db = DB::getConnection();
            if(isset($_POST['emailRegister']) && !empty($_POST['emailRegister'])){
                $issetUser = $db->fetchAssoc('SELECT id from users where email="'.$_POST['emailRegister'].'" OR name="'.$_POST['nicknameRegister'].'"');
                if(!empty($issetUser)){
                    return  new JsonResponse(json_encode(['error'=>'Такой пользователь уже существует.']));
                }
            }

            if((isset($_POST['emailRegister']) && !empty($_POST['emailRegister'])) && (isset($_POST['passwordRegister']) && !empty($_POST['passwordRegister']))){
                $user = new Users();
                $user->email = $_POST['emailRegister'];
                $user->password = md5($_POST['passwordRegister']);
                $user->approved = 0;
                $user->role = UsersModel::USER_ROLE;
                $user->rating = 0;
                if(isset($_POST['nicknameRegister']) && !empty($_POST['nicknameRegister'])){
                    $user->name = $_POST['nicknameRegister'];
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->sendConfirmMessage(['email'=>$user->email]);

                $db->executeQuery('update orders set user_id=:uid where email=:email',[':uid'=> $user->id, ':email'=>$user->email]);
                return  new JsonResponse(json_encode(['success'=>'1']));
            }


        return $this->render('ExchangerBundle:login:register.html.twig', ['javascriptOutput'=>$this->javascriptOutput]);
    }

    public function emailApprovingFromCabinetAction()
    {
        $this->sendConfirmMessage(['email'=>$_POST['userEmail']]);
        return  new JsonResponse(json_encode(['success'=>'1']));
    }

    public function recoveryPassAction()
    {
        if(isset($_POST['emailRecovery']) && !empty($_POST['emailRecovery'])){
            $db = DB::getConnection();
            $issetUser = $db->fetchAssoc('SELECT * from users where email="'.$_POST['emailRecovery'].'"');
            if(!empty($issetUser)){
                $this->sendRecoveryPasswordMesage($issetUser);
                return  new JsonResponse(json_encode(['success'=>'1']));
            } else {
                return  new JsonResponse(json_encode(['error'=>'1']));
            }
        }
    }

    public function recoveryPassActAction($hash)
    {
        $this->setJavascriptOutput('/js/backbone/controllers/BackboneRecoverypassActController.js');
        $this->setJavascriptOutput('/js/validator/validator.js/validator.min.js');

        if(isset($_POST['recoveryPassActSubmit'])){

            if(isset($_POST['newPass']) && !empty($_POST['newPass'])){
                $db = DB::getConnection();
                $user = $db->fetchAssoc('SELECT * from users where md5(email) = "'.$hash.'"');
                $userUpdate = $db->update('users', ['password'=>md5($_POST['newPass'])], ['id' => $user['id']]);
                if($userUpdate == true){
                    $this->sendRecoveryActPasswordMesage($user, $_POST['newPass']);
                    $this->get('session')->getFlashBag()->add('success', 'Пароль успешно изменен');
                    return $this->redirectToRoute('exchanger_recoverypass_action',['hash'=>$hash]);
                }
            }
        }

        return $this->render('ExchangerBundle:login:recoveryPassAct.html.twig', ['javascriptOutput'=>$this->javascriptOutput]);
    }

    public function emailApprovingAction($hash)
    {
        $db = DB::getConnection();
        $user = $db->fetchAssoc('SELECT * from users where md5(email) = "'.$hash.'"');
        $userUpdate = $db->update('users', ['approved'=>1], ['id' => $user['id']]);
        if($userUpdate == true){
            $approvedMessage = 'Email успешно подтвержден.';
        } else {
            $approvedMessage = 'Email не подтвержден или пользователя с такими данными не существует.';
        }

        return $this->render('ExchangerBundle:login:emailApproved.html.twig', ['approvedMessage'=>$approvedMessage]);
    }

    public function sendRecoveryPasswordMesage($user){
        $mail = new PHPMailer();
        try {
            $mail->isMail();
            $mail->setFrom(Constants::PROJECT_ORDER_MAIL, Constants::PROJECT_NAME);
            $mail->addAddress($user['email']);     // Add a recipient
            $mail->addReplyTo('goldbuy@noreply.com', 'Info');
            $mail->CharSet = 'UTF-8';
            $mail->addCustomHeader('Precedence','bulk');
            $unsubHref = Constants::PROJECT_PROTOCOL_FULL.'gold-buy.ru/unsubscribemail';
            $mail->addCustomHeader('List-Unsubscribe','<'.$unsubHref.'>');

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = '=?UTF-8?B?'.base64_encode('Изменение данных доступа '.Constants::PROJECT_NAME).'?=';
            $mail->Body    = 'Вы сделали запрос на смену пароля. <br /> Для смены пароля пройдите по <a href="'.Constants::PROJECT_PROTOCOL_FULL.'gold-buy.ru/recoverypassact/'.md5($user['email']).'">ссылке</a>.';

            $mail->send();
        } catch (Exception $e) {

        }
    }

    public function sendRecoveryActPasswordMesage($user, $password){
        $mail = new PHPMailer();
        try {
            $mail->isMail();
            $mail->setFrom(Constants::PROJECT_ORDER_MAIL, Constants::PROJECT_NAME);
            $mail->addAddress($user['email']);     // Add a recipient
            $mail->addReplyTo('goldbuy@noreply.com', 'Info');
            $mail->CharSet = 'UTF-8';
            $mail->addCustomHeader('Precedence','bulk');
            $unsubHref = Constants::PROJECT_PROTOCOL_FULL.'gold-buy.ru/unsubscribemail';
            $mail->addCustomHeader('List-Unsubscribe','<'.$unsubHref.'>');

            //Content
            $mail->isHTML(true);
            $mail->Subject = '=?UTF-8?B?'.base64_encode('Пароль успешно изменен'.Constants::PROJECT_NAME).'?=';
            $mail->Body    = 'Пароль успешно изменен. <br /> Ваш новый пароль '.$password.'.';

            $mail->send();
        } catch (Exception $e) {

        }
    }

    public function sendConfirmMessage($user){
        $mail = new PHPMailer();
        try {
            $mail->isMail();
            $mail->setFrom(Constants::PROJECT_ORDER_MAIL, Constants::PROJECT_NAME);
            $mail->addAddress($user['email']);     // Add a recipient
            $mail->addReplyTo('goldbuy@noreply.com', 'Info');
            $mail->CharSet = 'UTF-8';
            $mail->addCustomHeader('Precedence','bulk');
            $unsubHref = Constants::PROJECT_PROTOCOL_FULL.'gold-buy.ru/unsubscribemail';
            $mail->addCustomHeader('List-Unsubscribe','<'.$unsubHref.'>');

            //Content
            $mail->isHTML(true);
            $mail->Subject = '=?UTF-8?B?'.base64_encode('Подтверждение регистрации '.Constants::PROJECT_NAME).'?=';
            $mail->Body    = 'Для подтверждения регистрации пройдите по <a href="'.Constants::PROJECT_PROTOCOL_FULL.'gold-buy.ru/emailapproving/'.md5($user['email']).'">ссылке</a>.';

            $mail->send();
        } catch (Exception $e) {

        }
    }

    public function renderLoginFormAction()
    {
        return $this->render('ExchangerBundle:login:loginform.html.twig');
    }
}