<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Models\CurrencyModel;
use EntityBundle\Entity\Models\UsersModel;
use EntityBundle\Entity\Orders;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CabinetController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        if ($this->user == null && strpos($_SERVER['REQUEST_URI'], 'cabinet') != false ){
            $t = new RedirectResponse('/', 302);
            $t->sendHeaders();
            exit;
        }
    }

    public function indexAction()
    {
        $statuses = Orders::$_STATUSES;
        $currencies = CurrencyModel::$indexToCurrency;
        $orders = Orders::getOrdersList(50, $this->user->id);
        return $this->render('ExchangerBundle:cabinet:index.html.twig', ['orders'=>$orders, 'statuses'=>$statuses, 'currencies'=>$currencies]);
    }

    public function profileAction()
    {

        $this->setJavascriptOutput('/js/backbone/controllers/BackboneCabinetProfileController.js');
//        $this->setJavascriptOutput('/js/validator/validator.min.js');

        if(isset($_POST['submitProfile']) || !empty($_POST['submitProfile'])){
            if(!isset($_POST['email']) || empty($_POST['email'])){
                $this->get('session')->getFlashBag()->add('error', 'E-Mail не может быть пустым');
                return $this->render('ExchangerBundle:cabinet:profile.html.twig', ['user'=>$this->user, 'javascriptOutput'=>$this->javascriptOutput]);
            }

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('EntityBundle:Users')->find($this->user->id);

            if(isset($_POST['nickname']) && !empty($_POST['nickname']))         $user->name = $_POST['nickname'];
            if(isset($_POST['password']) && !empty($_POST['password']))         $user->password = md5($_POST['password']);
            if(isset($_POST['telegram']) && !empty($_POST['telegram']))         $user->telegram = $_POST['telegram'];
            if(isset($_POST['icq']) && !empty($_POST['icq']))                   $user->icq = $_POST['icq'];
            if(isset($_POST['skype']) && !empty($_POST['skype']))               $user->skype = $_POST['skype'];


            if(isset($_POST['email']) && !empty($_POST['email'])){
                $user->email = $_POST['email'];
                if($this->user->email != $user->email){
                  $user->approved = 0;
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $userToSet = $em->getRepository('EntityBundle:Users')->find($this->user->id);
            UsersModel::setUser($userToSet);

            $this->user = $userToSet;
            $this->get('session')->getFlashBag()->add('success', 'Данные успешно изменены.');
        }

        return $this->render('ExchangerBundle:cabinet:profile.html.twig', ['user'=>$this->user, 'javascriptOutput'=>$this->javascriptOutput]);
    }

}