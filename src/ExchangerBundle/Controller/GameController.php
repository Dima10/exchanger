<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace ExchangerBundle\Controller;

use EntityBundle\Entity\Constants;
use EntityBundle\Entity\CurrencyCourse;
use EntityBundle\Entity\Games;
use EntityBundle\Entity\GameSide;
use EntityBundle\Entity\Models\DB;
use EntityBundle\Entity\Models\OrdersModel;
use EntityBundle\Entity\Models\PaymentSystemsModel;
use EntityBundle\Entity\Models\TelegramModel;
use EntityBundle\Entity\Orders;
use EntityBundle\Entity\Servers;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\HttpFoundation\Request;

class GameController extends BaseController
{
    public function gameAction(Games $game)
    {
        $this->setJavascriptOutput('/js/backbone/controllers/BackboneGamePageController.js');
        $this->setJavascriptOutput('/js/validator/validator.min.js');
        $this->setJavascriptOutput('/js/backbone/components/jquery.mask.js');

        $gameIdForProjects = $game->id;
        $gameIdForSides = $game->id;
        $gameIdForServers = $game->id;
        if($game->isFreeShard()){
            $gameIdForProjects = $game->parent;
            $gameIdForSides = $game->parent;
            $gameIdForServers = $game->parent;
        }
        $projects = Games::getChildrenById($gameIdForProjects);
        $sides = GameSide::getSideByGameId($gameIdForSides);
        $servers = Servers::getServersByGameId($gameIdForServers);

        $currencyCourse = [];
        foreach (CurrencyCourse::getCurrencyCourse() as $currencyConfig){
            $currencyCourse[$currencyConfig['id']] = $currencyConfig['course'];
        }

        $discount = 0;
        if(!empty($this->user)){
            $discount = OrdersModel::getDiscountForUser($this->user->id);
        }

        $coinName = $game->getNameOfCoin($game);
        $gameName = $game->name;
        if($game->parent != 0){
            $gameParentName = DB::getConnection()->fetchColumn('Select name from games where id = '.$game->parent.'');
            $gameName = $gameParentName.' : '.$gameName;
        }
        $this->seoMeta->setData([
            'seoTitle' => $gameName.'.',
            'seoKeywords' => $game->name.', покупка, '.$coinName,
            'seoDescription' => 'Покупка игровых ценностей в игре '.$gameName.', на Gold-Buy.ru',
        ]);

        return $this->render('ExchangerBundle:game:game.html.twig',
            array(
                'game'=>$game,
                'projects'=>$projects,
                'servers'=>$servers,
                'sides'=>$sides,
                'currencyCourse'=>json_encode($currencyCourse),
                'payments'=> PaymentSystemsModel::getAllPaymentSystems(),
                'paymentsJson'=> json_encode(PaymentSystemsModel::getAllPaymentSystems()),
                'isFreeShard'=> $game->isFreeShard(),
                'coinName'=> $coinName,
                'javascriptOutput'=>$this->javascriptOutput,
                'gamePageId'=> ($game->isFreeShard()) ? $game->parent : $game->id,
                'discount'=> $discount,
            )
        );
    }

    public function orderCreateAction()
    {

        if(!isset($_POST['email']) || empty($_POST['email'])){
            return $this->redirectToRoute('exchanger_homepage');
        }

        $order = new Orders();
        if(isset($_POST['nickname']))                       $order->nickname = $_POST['nickname'];
        if(isset($_POST['email']))                          $order->email = $_POST['email'];
        if($this->user != null)                             $order->userId = $this->user->id;
        if(isset($_POST['gamePageId']))                     $order->mainGameId = $_POST['gamePageId'];
        if(isset($_POST['gameId']))                         $order->gameId = $_POST['gameId'];
        if(isset($_POST['serverSelect']))                   $order->serverId = $_POST['serverSelect'];
        if(isset($_POST['sideSelect']))                     $order->sideId = $_POST['sideSelect'];
        if(isset($_POST['paymentSelect']))                  $order->paymentId = $_POST['paymentSelect'];
        if(isset($_POST['currencySelect']))                 $order->currencyId = $_POST['currencySelect'];
        if(isset($_POST['paymentAmount']))                  $order->paymentAmount = $_POST['paymentAmount'];
        if(isset($_POST['purchasedAmount']))                $order->purchasedAmount = $_POST['purchasedAmount'];
        if(isset($_POST['comment']))                        $order->comment = $_POST['comment'];

        $order->hash = md5($order->email.$order->nickname.$order->date.$order->paymentId.$order->currencyId.$order->serverId.$order->gameId.$order->userId.microtime());

        $order->date = time();
        $order->status = 0;

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        $this->sendOrderMesageToAdmin($order);
        $this->sendOrderMesageToCustomer($order);

        $telegram = new TelegramModel(TelegramModel::EXCHANGER_GROUP_CHAT_ID);
        $telegram->sendMessage('Новый заказ номер '.$order->id.' '.$this->protocol.$_SERVER['HTTP_HOST'].'/admin/orders/show/'.$order->id);

        $orderInfo = OrdersModel::getInfoOrderById($order->id);

        return $this->render('ExchangerBundle:game:orderCreate.html.twig',
            array(
                'orderInfo' => $orderInfo
            )
        );
    }

    public function orderExistAction(Orders $order)
    {
        $orderInfo = OrdersModel::getInfoOrderById($order->id);

        return $this->render('ExchangerBundle:game:orderCreate.html.twig',
            array(
                'orderInfo' => $orderInfo
            )
        );
    }

    public function sendOrderMesageToCustomer(Orders $order){
        //https://medium.com/@alfakrai/%D0%B2%D0%BC%D0%B5%D0%BD%D1%8F%D0%B5%D0%BC%D0%B0%D1%8F-%D0%B8%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F-%D0%BA-phpmailer-51bf4530e2e4
        //https://github.com/PHPMailer/PHPMailer
        $mail = new PHPMailer();
        try {
            $mail->isMail();                                      // Set mailer to use SMTP
            $mail->setFrom(Constants::PROJECT_ORDER_MAIL, Constants::PROJECT_NAME);
            $mail->addAddress($order->email);     // Add a recipient
            $mail->addReplyTo('goldbuy@example.com', 'Info');
            $mail->CharSet = 'UTF-8';

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = '=?UTF-8?B?'.base64_encode('Новый заказ сервиса обмена валюты '.Constants::PROJECT_NAME).'?=';
            $mail->Body    = "Вы сделали новый заказ на сервисе обмена валют, ожидайте на ответ менеджера, ваш заказ №".$order->id.'. <br /> Для просмотра заказа пройдите по <a href="/game/order/exist/'.$order->hash.'">ссылке</a>.';

            $mail->send();
//            echo 'Message has been sent';
        } catch (Exception $e) {
//            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public function sendOrderMesageToAdmin(Orders $order){
        return;
        $mail = new PHPMailer();
        try {
            $mail->isMail();                                      // Set mailer to use SMTP
            $mail->setFrom(Constants::PROJECT_ORDER_MAIL, Constants::PROJECT_NAME);
            foreach (Constants::$adminOrderEmails as $adminMail){
                $mail->addAddress($adminMail);
            }
            $mail->addReplyTo('goldbuy@example.com', 'Info');

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = '=?UTF-8?B?'.base64_encode('Новый заказ сервиса обмена валюты '.Constants::PROJECT_NAME).'?=';
            $mail->Body    = 'Заказ <a href="/game/order/exist/'.$order->hash.'">'.$order->id.'</a>.';

            $mail->send();
//            echo 'Message has been sent';
        } catch (Exception $e) {
//            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }
}