<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\Games;
use EntityBundle\Entity\Helpers\StructureFilesHelper;
use EntityBundle\Entity\Models\Files;
use EntityBundle\Entity\Models\FilesImages;
use EntityBundle\Entity\Models\ServerModel;
use EntityBundle\Entity\StaticPages;
use Symfony\Component\HttpFoundation\Request;

class StaticPagesController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pages = $em->getRepository('EntityBundle:StaticPages')->findAll();

        return $this->render('AdminBundle:StaticPages:index.html.twig', array(
            'pages' => $pages,
        ));
    }

    /**
     * Displays a form to edit an existing game entity.
     *
     */
    public function editAction(Request $request, StaticPages $page)
    {

        if (isset($_POST['editpage'])) {

            if(isset($_POST['text']))                           $page->text = $_POST['text'];
            if(isset($_POST['seo_title']))                      $page->seoTitle = $_POST['seo_title'];
            if(isset($_POST['seo_keywords']))                   $page->seoKeywords = $_POST['seo_keywords'];
            if(isset($_POST['seo_description']))                $page->seoDescription = $_POST['seo_description'];

//            $this->filesUpload($page);

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_staticpages_edit', array('id' => $page->id));
        }

        $imageFiles = glob(StructureFilesHelper::getFileUploadDirectory().'/image/staticpage/'.$page->id.'/*.{jpg,png,gif}', GLOB_BRACE);

        $images = [];
        foreach ($imageFiles as $k => $imageFile){
            $images[] = substr($imageFile,strrpos($imageFile,'/')+1,strlen($imageFile));
        }

        return $this->render('AdminBundle:StaticPages:edit.html.twig', array(
            'page' => $page,
            'images' => $images,
        ));
    }


    public function filesUpload(Games $game)
    {

        if(!empty($_FILES['logo']['name'])){
            $_FILES['logo']['name'] = Files::transliterate($_FILES['logo']['name']);

            $file = new FilesImages($_FILES['logo']);
            $file->upload($_FILES['logo']);
            if ($file->uploaded) {
//                    $handle->file_new_name_body   = 'image_resized';
//                    $handle->image_resize         = true;
//                    $handle->image_x              = 100;
//                    $handle->image_ratio_y        = true;
                $file->process(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/');
                if ($file->processed) {
                    @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/'.$game->logo);
                    $game->logo = $file->file_dst_name;
                    $file->clean();
                } else {
                    echo 'error : ' . $file->error;
                }
            }
        }
    }

    public function delimgAction($id, $imgtype)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em->getRepository('EntityBundle:Games')->find($id);
        if($imgtype == 'logo'){
            @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/'.$game->logo);
            $game->logo = '';
        } else {
            @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/games/'.$game->biglogo);
            $game->biglogo = '';
        }
        $em->flush();

        return $this->redirectToRoute('admin_games_edit', ['id'=>$game->id]);
    }

}
