<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\GameSide;
use EntityBundle\Entity\Helpers\StructureFilesHelper;
use EntityBundle\Entity\Models\DB;
use ExchangerBundle\Extensions\simple_html_dom;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends BaseAdminController
{

    public function getListGamesAction()
    {


        $db = DB::getConnection();
        $rows = $db->fetchAll('SELECT id, name from games where name like "%'.$_GET['term'].'%"');

        $arrReturn = [];
        foreach ($rows as $row){
            $arrReturn[] = [
                'id' => $row['id'],
                'value' => $row['name']
            ];
        }

        return  new JsonResponse(json_encode($arrReturn));
    }


    public function uploadFileAction(Request $request)
    {

        $data = $_POST['imageSrcBase64'];

        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new \Exception('invalid image type');
            }

            $data = base64_decode($data);

            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }
        $path = StructureFilesHelper::getFileUploadDirectory();
        $savePath = $path.$_POST['savePath'];
        if(!file_exists(dirname($savePath))){
            mkdir(dirname($savePath), 0777, true);
            chmod(dirname($savePath), 0777);
        }
        file_put_contents($savePath, $data);

        return  new JsonResponse(json_encode([
            'link'=> $_POST['savePath'],
            'delLink'=>$_POST['savePath'],
        ]));
    }

    public function delimgAction()
    {

        unlink(StructureFilesHelper::getFileUploadDirectory().$_POST['delImgSrc']);
        return  new JsonResponse(json_encode([]));
    }

    public function delGameSideAction()
    {

        $db = DB::getConnection();
        $db->delete('game_side', ['id' =>$_POST['sideid']]);

        return  new JsonResponse(json_encode($_POST['sideid']));
    }

    public function getDataForGeneralPricePageAction()
    {
        $baseArray[] = 'https://funpay.ru/chips/1/';
        $baseArray[] = 'https://funpay.ru/chips/2/';
        $baseArray[] = 'https://funpay.ru/chips/3/';
        $baseArray[] = 'https://funpay.ru/chips/4/';
        $baseArray[] = 'https://funpay.ru/chips/6/';
        $baseArray[] = 'https://funpay.ru/chips/8/';
        $baseArray[] = 'https://funpay.ru/chips/9/';
        $baseArray[] = 'https://funpay.ru/chips/10/';
        $baseArray[] = 'https://funpay.ru/chips/11/';
        $baseArray[] = 'https://funpay.ru/chips/12/';
        $baseArray[] = 'https://funpay.ru/chips/14/';
        $baseArray[] = 'https://funpay.ru/chips/15/';
        $baseArray[] = 'https://funpay.ru/chips/16/';
        $baseArray[] = 'https://funpay.ru/chips/17/';
        $baseArray[] = 'https://funpay.ru/chips/19/';
        $baseArray[] = 'https://funpay.ru/chips/23/';
        $baseArray[] = 'https://funpay.ru/chips/30/';
        $baseArray[] = 'https://funpay.ru/chips/34/';
        $baseArray[] = 'https://funpay.ru/chips/35/';
        $baseArray[] = 'https://funpay.ru/chips/37/';
        $baseArray[] = 'https://funpay.ru/chips/39/';
        $baseArray[] = 'https://funpay.ru/chips/42/';
        $baseArray[] = 'https://funpay.ru/chips/55/';
        $baseArray[] = 'https://funpay.ru/chips/64/';
        $baseArray[] = 'https://funpay.ru/chips/92/';

        $hayStackServers = [
            9 => 'Asterios.tm Asterios x5',
            8 => 'Asterios.tm Hunter x55',
            7 => 'Asterios.tm Phoenix x7',
            4 => 'Scryde.ru HF x100',
            10 => 'Scryde.ru HF x50',
            12 => 'L2E-Global.com (Averia.ws) x7',
            13 => 'Euro-PvP.com x100 [OLD]',
            14 => 'Euro-PvP.com x100 [NEW]',
            15 => 'GameCoast.net Helheim x10',
            16 => 'GameCoast.net Midgard x3',
            17 => 'KetraWars.net x1 NEW (цена за 1к)',
            18 => 'KetraWars.net x100 OLD',

        ////            'l2e-global.com x25',
            21 => 'La2Dream.su x50 NEW',
            22 => 'Rampage.pw Craft-PvP x10',
            23 => 'Rampage.pw MultiCraft x100',
            24 => 'RPG-Club.com Президент x3',
            29 => 'zMega.com Asia x20',
        //// 30 =>          'zMega.com Bestia x50',
            31 => 'zMega.com Mega x10',
            38 => 'Airin + Erica',
            39 => 'Athebaldt + Esthus',
            40 => 'Blackbird + Ramsheart',
            41 => 'Elcardia + Cadmus',
            42 => 'Hatos',
            33 => 'Anakim',
            34 => 'Gran Kain',
            35 => 'Lilith',
            36 => 'Paagrio',
            37 => 'Shillien',
            43 => 'Elysium-Project.org Nighthaven PvP x1',
            44 => 'Epicwow.com Legion x1',
            45 => 'Firestorm-servers.com Sylvanas PvE x3',
            46 => 'Kronos-WoW.com KRONOS (цена за 1 золотой)',
            47 => 'Kronos-WoW.com KRONOS III (цена за 1 золотой)',
            48 => 'Lightshope.org Lightbringer (цена за 1 золотой)',
            49 => 'Lightshope.org Northdale (цена за 1 золотой)',
            50 => 'PandaWoW.ru x100',
            51 => 'Sirus.su Frostmourne x1',
            52 => 'Sirus.su Scourge x2 - 3.3.5a+',
            53 => 'Sirus.su Sirus x10 - 3.3.5a+ - 3.3.5а+',
            54 => 'Sunwell.pl Angrathar',
            55 => 'Sunwell.pl Nightbane (цена за 1 золотой)',
            56 => 'Uwow.biz x100 - 7.3.5',
            57 => 'Uwow.biz x100 - 5.4.8',
            58 => 'Warmane.com Icecrown',
            59 => 'Warmane.com Lordaeron',
            60 => 'Warmane.com Outland (цена за 1 золотой)',
            61 => 'WoWCircle.com 7.3.5 x100',
            62 => 'WoWCircle.com 7.3.5 x4',
            63 => 'WoWCircle.com logon 3.3.5a x10',
            64 => 'WoWCircle.com logon 3.3.5a x100',
            65 => 'WoWCircle.com logon 3.3.5a x5',
            66 => 'WoWCircle.com logon 4.3.4 x100',
            67 => 'WoWCircle.com logon3 x1 PvE Virgin',
            68 => 'Азурегос',
            69 => 'Борейская тундра',
            70 => 'Вечная Песня',
            71 => 'Галакронд',
            72 => 'Голдринн',
            73 => 'Гордунни',
            74 => 'Гром',
            75 => 'Дракономор',
            76 => 'Пиратская Бухта',
            77 => 'Король-лич',
            78 => 'Подземье',
            79 => 'Ревущий фьорд',
            80 => 'Свежеватель Душ',
            81 => 'Страж Смерти',
            82 => 'Черный Шрам',
            83 => 'Ясеневый лес',
            87 => 'AionLegend.im',
            84 => 'GameCoast.net Hildr',
            85 => 'GameCoast.net Kassiel',
            88 => 'GoldAion.com',
            86 => 'TheAion.ru',
            89 => 'Асгард',
            90 => 'Этерия',
            91 => 'Луций',
            92 => 'Кипроза',
            93 => 'Мелисара',
            94 => 'Невер',
            95 => 'Гартарейн',
            96 => 'Левиафан',
            97 => 'Ария',
            98 => 'Иштар',
            99 => 'Хазе - Fresh Start',
            100 => 'Эрнард',
            101 => 'Морфеос',
            102 => 'Марли',
            103 => 'Ашьяра',
            104 => 'Гленн',
            105 => 'Лорея',
            106 => 'Albion (live)',
            107 => 'Вечный Зов',
            108 => 'Молодая Гвардия',
            109 => 'Наследие Богов',
            110 => 'Нить судьбы',
            111 => 'Аметист',
            112 => 'Изумруд',
            113 => 'Рубин',
            114 => 'Сапфир',
            115 => '(EU) Harman',
            116 => 'Tranquility',
            117 => '(EU)',
            118 => 'Мидлас',
            119 => 'Экзаран',
            120 => 'Русский',
            121 => '(PC) Betrayal',
            122 => '(PC) Hardcore',
            123 => '(PC) Hardcore Betrayal',
            124 => '(PC) Standard',
            125 => 'Ворон',
            126 => 'Дракон',
            127 => 'Саргас',
            128 => 'Титан',
            129 => 'Электра',
            130 => 'Метеос',
            131 => 'Югенес',
        //// 132 =>           'Акари',
            133 => 'Атум',
            134 => 'Кенсай',
            135 => 'Раэлис',
            136 => '(RU) Гранас',
            137 => '(RU) Логрус',
            138 => '(RU) Энигма',
            139 => 'PC (EU) - Killian [PvP]',
            140 => 'PC (EU) - Mystel [PvE]',
            141 => 'PC (EU) - Seren [PvE]',
            142 => 'PC (EU) - Yurian [PvE]',
            143 => 'Europe (PC/Mac – EU)',
            144 => 'North America (PC/Mac – NA)',
            145 => 'Warframe.com'
        ];

        $resultArray = [];
        $priceString = null;
        foreach ($baseArray as $baseUrl) {

            $str = $this->curlUrl($baseUrl);

            $html = new simple_html_dom();
            $html->load($str);

            $priceString = $html->find('table[class="table table-condensed table-hover table-clickable showcase-table table-sortable"] thead tr')[0]->lastChild()->plaintext;

            $servers = [];
            foreach($html->find('table[class="table table-condensed table-hover table-clickable showcase-table table-sortable"] tbody tr') as $tr){
                if(isset($servers[$tr->firstChild()->plaintext]['count']) && $servers[$tr->firstChild()->plaintext]['count']>=5){
                    continue;
                }
                $servers[$tr->firstChild()->plaintext] = [
                    'count'=> isset($servers[$tr->firstChild()->plaintext]['count']) ? $servers[$tr->firstChild()->plaintext]['count']+1 : 1,
                    'amount'=> isset($servers[$tr->firstChild()->plaintext]['amount']) ? $servers[$tr->firstChild()->plaintext]['amount']+floatval($tr->lastChild()->plaintext) : floatval($tr->lastChild()->plaintext),
                ];
            }


            foreach ($servers as $server => $serverInfo){
                if(in_array(trim($server), $hayStackServers)){
                    $divider = $this->getDivider($priceString);
                    if(strpos($server, '(цена за 1 золотой)') != false){
                        $divider = 1;
                    }
                    if(strpos($server, '(цена за 1к)') != false){
                        $divider = 1000;
                    }
                    $resultArray[array_search($server, $hayStackServers)] = [ 'name' => $server, 'cost' => round((($serverInfo['amount']/$serverInfo['count'])/$divider),8)*1.25];
                }
            }

        }



        return  new JsonResponse(json_encode($resultArray));
    }

    protected function curlUrl($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($curl);
        curl_close($curl);

        return $str;
    }

    protected function getDivider($priceString){
        $divider = 1;
        if(trim($priceString) == 'цена за 1 к' || trim($priceString) == 'цена за 1000'){
            $divider = 1000;
        } elseif (trim($priceString) == 'цена за 1 кк'){
            $divider = 1000000;
        } elseif (trim($priceString) == 'цена за 1 ккк' || trim($priceString) == 'цена за 1000 кк'){
            $divider = 1000000000;
        }

        return $divider;
    }

}
