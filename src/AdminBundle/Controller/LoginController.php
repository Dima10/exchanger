<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\Users;

class LoginController extends BaseAdminController
{
    public function loginAction()
    {

        if (isset($_COOKIE['adminUser'])){
            return $this->redirectToRoute('admin_homepage');
        }

        if(!empty($_POST['email']) && !empty($_POST['pass'])){

            $user = $this->getDoctrine()
                ->getRepository(Users::class)
                ->findOneBy(['email'=>$_POST['email'], 'password'=>md5($_POST['pass']), 'role'=>'admin']);

            if($user != null ){
                setcookie ("adminUser", base64_encode(serialize($user)), time()+(3600*5));
                setcookie ("adminUserName", $user->name, time()+(3600*5));
                return $this->redirectToRoute('admin_homepage');
            }
        }

        return $this->render('AdminBundle:Login:login.html.twig');
    }

    public function logoutAction()
    {
        setcookie ("adminUser", false, -5);
        return $this->redirect('/');
    }
}
