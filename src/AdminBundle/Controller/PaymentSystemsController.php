<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\Games;
use EntityBundle\Entity\Models\Files;
use EntityBundle\Entity\Models\FilesImages;
use EntityBundle\Entity\Models\PaymentSystemsModel;
use EntityBundle\Entity\Models\ServerModel;
use Symfony\Component\HttpFoundation\Request;

class PaymentSystemsController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */
    public function indexAction()
    {
        $paymentSytems = PaymentSystemsModel::getAllPaymentSystems();
        return $this->render('AdminBundle:PaymentSystems:index.html.twig', array(
            'paymentSytems' => $paymentSytems,
        ));
    }
}
