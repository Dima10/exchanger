<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\Games;
use EntityBundle\Entity\Models\Files;
use EntityBundle\Entity\Models\FilesImages;
use EntityBundle\Entity\Seo;
use EntityBundle\Entity\Servers;
use Symfony\Component\HttpFoundation\Request;

class SeoController extends BaseAdminController
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if(isset($_POST['serchSubmit']) && !empty($_POST['serchSubmit'])){
            $seo = $em->getRepository('EntityBundle:Seo')->findBy(['url'=>trim($_POST['url'])]);
        } else {
            $seo = $em->getRepository('EntityBundle:Seo')->findAll();
        }

        return $this->render('AdminBundle:Seo:index.html.twig', array(
            'seo' => $seo,
        ));
    }

    /**
     * Creates a new servers entity.
     *
     */
    public function addAction(Request $request)
    {
        $seo = new Seo();

        if (isset($_POST['editSeo'])) {
            if(isset($_POST['url']))                $seo->url = $_POST['url'];
            if(isset($_POST['seo_title']))          $seo->seoTitle = $_POST['seo_title'];
            if(isset($_POST['seo_keywords']))       $seo->seoKeywords = $_POST['seo_keywords'];
            if(isset($_POST['seo_description']))    $seo->seoDescription = $_POST['seo_description'];
            if(isset($_POST['seo_h1']))             $seo->seoH1 = $_POST['seo_h1'];
            $em = $this->getDoctrine()->getManager();
            $em->persist($seo);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_seo_edit', array('id' => $seo->id));
        }

        return $this->render('AdminBundle:Seo:edit.html.twig', array(
            'seo' => $seo,
            'isNew' => 1,
        ));
    }

    /**
     * Displays a form to edit an existing seo entity.
     *
     */
    public function editAction(Request $request, Seo $seo)
    {

        if (isset($_POST['editSeo'])) {
            if(isset($_POST['url']) && !empty($_POST['url']))                           $seo->url = $_POST['url'];
            if(isset($_POST['seo_title']) && !empty($_POST['seo_title']))               $seo->seoTitle = $_POST['seo_title'];
            if(isset($_POST['seo_keywords']) && !empty($_POST['seo_keywords']))         $seo->seoKeywords = $_POST['seo_keywords'];
            if(isset($_POST['seo_description']) && !empty($_POST['seo_description']))   $seo->seoDescription = $_POST['seo_description'];
            if(isset($_POST['seo_h1']) && !empty($_POST['seo_h1']))                     $seo->seoH1 = $_POST['seo_h1'];

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_seo_edit', array('id' => $seo->id));
        }

        return $this->render('AdminBundle:Seo:edit.html.twig', array(
            'seo' => $seo,
        ));
    }
}
