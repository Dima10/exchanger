<?php

namespace AdminBundle\Controller;

use ExchangerBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PaymentsController extends BaseAdminController
{

    public function indexAction()
    {
        return $this->render('AdminBundle:Payments:index.html.twig');
    }

}
