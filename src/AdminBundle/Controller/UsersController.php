<?php

namespace AdminBundle\Controller;

use ExchangerBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UsersController extends BaseAdminController
{

    public function indexAction()
    {
        return $this->render('AdminBundle:Users:index.html.twig');
    }

}
