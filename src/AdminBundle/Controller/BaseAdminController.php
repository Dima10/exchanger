<?php
/**
 * Created by PhpStorm.
 * User: alastar
 * Date: 28.11.18
 * Time: 0:02
 */

namespace AdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BaseAdminController extends Controller
{
    public $adminUser;

    public function __construct()
    {
        if (!isset($_COOKIE['adminUser']) && $_SERVER['REQUEST_URI'] != '/admin/login'){
            $t = new RedirectResponse('/admin/login', 302);
            $t->sendHeaders();
            exit;
        }

        if (isset($_COOKIE['adminUser'])){
            $this->adminUser = unserialize(base64_decode($_COOKIE['adminUser']));
        }
    }
}