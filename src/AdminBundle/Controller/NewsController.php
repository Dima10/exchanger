<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\Helpers\StructureFilesHelper;
use EntityBundle\Entity\Models\Files;
use EntityBundle\Entity\Models\FilesImages;
use EntityBundle\Entity\News;
use ExchangerBundle\CustomModels\SeoMeta;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $news = $em->getRepository('EntityBundle:News')->findBy(['deleted'=>0]);

        return $this->render('AdminBundle:News:index.html.twig', array(
            'news' => $news,
        ));
    }

    /**
     * Creates a new game entity.
     *
     */
    public function addAction()
    {
        $new = new News();

        $new->name = 'Новая новость';
        $new->text = '';

        $new->createdDate = (new \DateTime())->setTimestamp(time());
        $new->href = time();
        $new->seoH1 = '';
        $new->seoTitle = '';
        $new->seoKeywords = '';
        $new->seoDescription = '';
        $new->active = 0;
        $new->deleted = 0;
        $em = $this->getDoctrine()->getManager();
        $em->persist($new);
        $em->flush();

        return $this->redirectToRoute('admin_news_edit', array('id' => $new->id));
    }

    /**
     * Displays a form to edit an existing game entity.
     *
     */
    public function editAction(Request $request, News $new)
    {

        if (isset($_POST['editnews']) || isset($_POST['previewnews'])) {

            if(isset($_POST['name']))               $new->name = $_POST['name'];
            if(isset($_POST['text']))               $new->text = $_POST['text'];
            if(isset($_POST['img']))                $new->img = $_POST['img'];
            if(isset($_POST['created_date']))       $new->createdDate = date('Y-d-m', time());
            if(isset($_POST['active']))             $new->active = (int)$_POST['active'];
            if(isset($_POST['href']))               $new->href = $_POST['href'];
            if(isset($_POST['seo_h1']))             $new->seoH1 = $_POST['seo_h1'];
            if(isset($_POST['seo_title']))          $new->seoTitle = $_POST['seo_title'];
            if(isset($_POST['seo_keywords']))       $new->seoKeywords = $_POST['seo_keywords'];
            if(isset($_POST['seo_description']))    $new->seoDescription = $_POST['seo_description'];
            if(isset($_POST['short_text']))         $new->shortText = $_POST['short_text'];

                if(isset($_POST['previewnews'])){

                    $seoMeta = SeoMeta::getSeometa();
                    $seoMeta->setData([
                        'seoTitle' => $new->seoTitle,
                        'seoKeywords' => $new->seoKeywords,
                        'seoDescription' => $new->seoDescription,
                    ]);

                    return $this->render('ExchangerBundle:news:new.html.twig',
                        array(
                            'new' => $new,
                            'adminPreview' => 1,
                        )
                    );

                }

            $this->filesUpload($new);

            $em = $this->getDoctrine()->getManager();
            $em->persist($new);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_news_edit', array('id' => $new->id));
        }

        $imageFiles = glob(StructureFilesHelper::getFileUploadDirectory().'/image/news/'.$new->id.'/*.{jpg,jpeg,png,gif}', GLOB_BRACE);

        $images = [];
        foreach ($imageFiles as $k => $imageFile){
            $images[] = substr($imageFile,strrpos($imageFile,'/')+1,strlen($imageFile));
        }

        return $this->render('AdminBundle:News:edit.html.twig', array(
            'new' => $new,
            'images' => $images,
        ));
    }

    /**
     * Disable a game entity.
     *
     */
    public function onofAction(Request $request, News $new)
    {
        $em = $this->getDoctrine()->getManager();
        if($new->active == 0 ){
            $new->active = 1;
        } else {
            $new->active = 0;
        }
        $em->flush();

        return $this->redirectToRoute('admin_news');
    }

    public function deleteAction(Request $request, News $new)
    {
        $em = $this->getDoctrine()->getManager();
        if($new->deleted == 0 ){
            $new->deleted = 1;
        } else {
            $new->deleted = 0;
        }
        $em->flush();

        $this->onofAction($request, $new);
        return $this->redirectToRoute('admin_news');
    }

    /**
     * Disable a game entity.
     *
     */
    public function delimgAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $new = $em->getRepository('EntityBundle:News')->find($id);
        @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/news/'.$new->id.'/'.$new->img);
        $new->img = '';
        $em->flush();

        return $this->redirectToRoute('admin_news_edit', ['id'=>$new->id]);
    }

    public function filesUpload(News $new)
    {

        if(!empty($_FILES['img']['name'])){
            $_FILES['img']['name'] = Files::transliterate($_FILES['img']['name']);

            $file = new FilesImages($_FILES['img']);
            $file->upload($_FILES['img']);
            if ($file->uploaded) {
//                    $handle->file_new_name_body   = 'image_resized';
//                    $handle->image_resize         = true;
//                    $handle->image_x              = 100;
//                    $handle->image_ratio_y        = true;
                $path = StructureFilesHelper::getFileUploadDirectory();
                $file->process($path.'/image/news/'.$new->id.'/');
                if ($file->processed) {
                    @unlink($path.'/image/widget/'.$new->id.'/'.$new->img);
                    $new->img = $file->file_dst_name;
                    $file->clean();
                } else {
                    echo 'error : ' . $file->error;
                }
            }
        }
    }
}
