<?php

namespace AdminBundle\Controller;

class ReviewsController extends BaseAdminController
{

    public function indexAction()
    {
        return $this->render('AdminBundle:Reviews:index.html.twig', array());
    }

}
