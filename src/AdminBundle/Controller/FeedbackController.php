<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\Constants;
use EntityBundle\Entity\Feedback;
use EntityBundle\Entity\Games;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\HttpFoundation\Request;

class FeedbackController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $feedbackMessages = $em->getRepository('EntityBundle:Feedback')->findBy([],['id'=>'desc'],100);

        return $this->render('AdminBundle:Feedback:index.html.twig', array(
            'feedbackMessages' => $feedbackMessages,
        ));
    }

    public function showAction(Feedback $feedback)
    {
        if(isset($_POST['answerSubmit'])){

            $mail = new PHPMailer();
            try {
                $mail->isMail();                                      // Set mailer to use SMTP
                $mail->setFrom(Constants::PROJECT_ORDER_MAIL, Constants::PROJECT_NAME);
                $mail->addAddress($feedback->email);     // Add a recipient
                $mail->addReplyTo('goldbuy@example.com', 'Info');

                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->CharSet = 'UTF-8';
                $mail->Subject = '=?UTF-8?B?'.base64_encode('Ответ на ваще обращение. '.Constants::PROJECT_NAME).'?=';
                $mail->Body    = $_POST['answer'];

                $mail->send();

                $feedback->answered = 1;
                $em = $this->getDoctrine()->getManager();
                $em->persist($feedback);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Задроту успешно отвечено');
                return $this->redirectToRoute('admin_feedback');
//            echo 'Message has been sent';
            } catch (Exception $e) {
//            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }
        }
        return $this->render('AdminBundle:Feedback:edit.html.twig', array(
            'feedback' => $feedback,
        ));
    }

    public function editAction(Request $request, Games $game)
    {

        if (isset($_POST['editgame'])) {

            $this->addChildren($game);
            $this->addSides($game);

            if(isset($_POST['name']) && !empty($_POST['name']))                 $game->name = $_POST['name'];
            if(isset($_POST['href']) && !empty($_POST['href']))                 $game->href = $_POST['href'];
            if(isset($_POST['name_of_coin']) && !empty($_POST['name_of_coin'])) $game->nameOfCoin = $_POST['name_of_coin'];
            if(isset($_POST['priority']) && !empty($_POST['priority']))         $game->priority = (int)$_POST['priority'];
            if(isset($_POST['parent']) && !empty($_POST['parent']))             $game->parent = (int)$_POST['parent'];
            (isset($_POST['hasChildren']) && !empty($_POST['hasChildren'])) ?   $game->hasChildren = (int)$_POST['hasChildren'] : $game->hasChildren = 0;
            (isset($_POST['active']) && !empty($_POST['active'])) ?             $game->active = (int)$_POST['active'] : $game->active = 0;

            $this->filesUpload($game);

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_games_edit', array('id' => $game->id));
        }

        $gameParent = $this->getDoctrine()->getManager()->getRepository('EntityBundle:Games')->find($game->parent);
        $childrenGames = $this->getDoctrine()->getManager()->getRepository('EntityBundle:Games')->findBy(['parent'=>$game->id]);

        $em = $this->getDoctrine()->getManager();
        $servers = $em->getRepository('EntityBundle:Servers')->findBy(['gameId'=>$game->id]);
        $sides = $em->getRepository('EntityBundle:GameSide')->findBy(['gameId'=>$game->id]);



        return $this->render('AdminBundle:Games:edit.html.twig', array(
            'game' => $game,
            'gameParent' => $gameParent,
            'childrenGames' => $childrenGames,
            'servers' => $servers,
            'sides' => $sides,
        ));
    }

}
