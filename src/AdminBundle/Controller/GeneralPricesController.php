<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\Models\DB;
use EntityBundle\Entity\Models\ServerModel;
use Symfony\Component\HttpFoundation\Request;

class GeneralPricesController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */
    public function indexAction()
    {

        $servers = ServerModel::getServersList();

        return $this->render('AdminBundle:GeneralPrices:index.html.twig', array(
            'servers' => $servers
        ));
    }

    public function editAction(Request $request)
    {

        DB::getConnection()->beginTransaction();
            foreach ($_POST['price_for_coin'] as $serverId => $price){
                echo $serverId.' - '.$price.' - '.$_POST['measure_unit'][$serverId];
                echo '<br />';
                DB::getConnection()->update('servers',['price_for_coin'=>$price, 'measure_unit'=>$_POST['measure_unit'][$serverId]],['id'=>$serverId]);
            }
        DB::getConnection()->commit();

        return $this->redirectToRoute('admin_generalprices');

    }

}
