<?php

namespace AdminBundle\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use EntityBundle\Entity\Games;
use EntityBundle\Entity\Helpers\StructureFilesHelper;
use EntityBundle\Entity\Models\DB;
use EntityBundle\Entity\Models\Files;
use EntityBundle\Entity\Models\FilesImages;
use EntityBundle\Entity\Models\GameModel;
use EntityBundle\Entity\Models\ServerModel;
use EntityBundle\Entity\Servers;
use Symfony\Component\HttpFoundation\Request;

class ServersController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */

    /**
     * Displays a form to edit an existing server entity.
     *
     */
    public function indexAction()
    {

        $servers = ServerModel::getServersList();

        return $this->render('AdminBundle:Servers:index.html.twig', array(
            'servers' => $servers,
        ));
    }

    /**
     * Creates a new servers entity.
     *
     */
    public function addAction(Request $request)
    {
        $server = new Servers();

        if (isset($_POST['editserver'])) {
            if(isset($_POST['name']))               $server->name = $_POST['name'];
            if(isset($_POST['game_id']))            $server->gameId = (int)$_POST['game_id'];
            if(isset($_POST['price_for_coin']))     $server->priceForCoin = $_POST['price_for_coin'];
            if(isset($_POST['active']))             $server->active = (int)$_POST['active'];
            if(isset($_POST['measure_unit']))       $server->measureUnit = $_POST['measure_unit'];
            $server->deleted = 0;
            $em = $this->getDoctrine()->getManager();
            $em->persist($server);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_servers_edit', array('id' => $server->id));
        }

        $em = $this->getDoctrine()->getManager();
        $games = $em->getRepository('EntityBundle:Games')->findBy(['hasChildren'=>0]);

        return $this->render('AdminBundle:Servers:edit.html.twig', array(
            'server' => $server,
            'games' => $games,
            'isNew' => 1,
        ));
    }

    /**
     * Displays a form to edit an existing servers entity.
     *
     */
    public function editAction(Request $request, Servers $server)
    {

        if (isset($_POST['editserver'])) {
            if(isset($_POST['name']) && !empty($_POST['name']))                         $server->name = $_POST['name'];
            if(isset($_POST['game_id']) && !empty($_POST['game_id']))                   $server->gameId = $_POST['game_id'];
            if(isset($_POST['price_for_coin']) && !empty($_POST['price_for_coin']))     $server->priceForCoin = $_POST['price_for_coin'];
            (isset($_POST['active']) && !empty($_POST['active'])) ?                     $server->active = (int)$_POST['active'] : $server->active = 0;
            if(isset($_POST['measure_unit']) && !empty($_POST['measure_unit']))         $server->measureUnit = $_POST['measure_unit'];

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_servers_edit', array('id' => $server->id));
        }

        $em = $this->getDoctrine()->getManager();
        $games = $em->getRepository('EntityBundle:Games')->findBy(['hasChildren'=>0]);

        return $this->render('AdminBundle:Servers:edit.html.twig', array(
            'server' => $server,
            'games' => $games,
        ));
    }

    /**
     * Disable a game entity.
     *
     */
    public function onofAction(Request $request, Servers $server)
    {
        $em = $this->getDoctrine()->getManager();
        if($server->active == 0 ){
            $server->active = 1;
        } else {
            $server->active = 0;
        }
        $em->flush();

        return $this->redirectToRoute('admin_servers');
    }

    public function deleteAction(Request $request, Servers $server)
    {
        $em = $this->getDoctrine()->getManager();
        if($server->deleted == 0 ){
            $server->deleted = 1;
        } else {
            $server->deleted = 0;
        }
        $em->flush();
        return $this->onofAction($request, $server);
    }

    /**
     * Disable a game entity.
     *
     */
    public function delimgAction($id, $imgtype)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em->getRepository('EntityBundle:Games')->find($id);
        if($imgtype == 'logo'){
            @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/'.$game->logo);
            $game->logo = '';
        } else {
            @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/games/'.$game->biglogo);
            $game->biglogo = '';
        }
        $em->flush();

        return $this->redirectToRoute('admin_servers_edit', ['id'=>$game->id]);
    }

    public function filesUpload(Games $game)
    {

        if(!empty($_FILES['logo']['name'])){
            $_FILES['logo']['name'] = Files::transliterate($_FILES['logo']['name']);

            $file = new FilesImages($_FILES['logo']);
            $file->upload($_FILES['logo']);
            if ($file->uploaded) {
//                    $handle->file_new_name_body   = 'image_resized';
//                    $handle->image_resize         = true;
//                    $handle->image_x              = 100;
//                    $handle->image_ratio_y        = true;
                $file->process(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/');
                if ($file->processed) {
                    @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/'.$game->logo);
                    $game->logo = $file->file_dst_name;
                    $file->clean();
                } else {
                    echo 'error : ' . $file->error;
                }
            }
        }


        //=====================================================================================
        if(!empty($_FILES['biglogo']['name'])){
            $_FILES['biglogo']['name'] = Files::transliterate($_FILES['biglogo']['name']);

            $file = new FilesImages($_FILES['biglogo']);
            $file->upload($_FILES['biglogo']);
            if ($file->uploaded) {
                $file->process(StructureFilesHelper::getFileUploadDirectory().'/image/games/');
                if ($file->processed) {
                    @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/games/'.$game->biglogo);
                    $game->biglogo = $file->file_dst_name;
                    $file->clean();
                } else {
                    echo 'error : ' . $file->error;
                }
            }
        }
    }

    public function updateListAction()
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($_POST['priority'] as $gameId => $priority){
            $game = $em->getRepository('EntityBundle:Games')->find($gameId);
            $game->priority = $priority;
            $em->flush();
        }

        return $this->redirectToRoute('admin_servers');
    }

    protected function addChildren(Games $gameParent){

        $em = $this->getDoctrine()->getManager();
            foreach ( current($_POST['children']) as $gameName){
                $game = new Games();
                $game->active = 0;
                $game->name = $gameName;
                $game->parent = $gameParent->id;
                $em->persist($game);
                $em->flush();
            }
        unset($em);
    }
}
