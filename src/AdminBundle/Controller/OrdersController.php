<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\GameProjects;
use EntityBundle\Entity\Games;
use EntityBundle\Entity\GameSide;
use EntityBundle\Entity\Models\CurrencyModel;
use EntityBundle\Entity\Models\Files;
use EntityBundle\Entity\Models\FilesImages;
use EntityBundle\Entity\Models\ServerModel;
use EntityBundle\Entity\Orders;
use Symfony\Component\HttpFoundation\Request;

class OrdersController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */
    public function indexAction()
    {
        $orders = Orders::getOrdersList();
        $currencies = CurrencyModel::$indexToCurrency;

        return $this->render('AdminBundle:Orders:index.html.twig', array(
            'orders' => $orders,
            'currencies' => $currencies,
        ));
    }

    /**
     * Displays a form to edit an existing game entity.
     *
     */
    public function showAction(Orders $order)
    {

        if (isset($_POST['editorder'])) {
            if(isset($_POST['status']) && !empty($_POST['status']))   $order->status = $_POST['status'];

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_orders_show', array('id' => $order->id));
        }


        $order = Orders::getOrderById($order->id);
        $currencies = CurrencyModel::$indexToCurrency;
        
        return $this->render('AdminBundle:Orders:edit.html.twig', array(
            'order' => $order,
            'statuses' => Orders::$_STATUSES,
            'currencies' => $currencies,
        ));
    }
}
