<?php

namespace AdminBundle\Controller;

use EntityBundle\Entity\GameProjects;
use EntityBundle\Entity\Games;
use EntityBundle\Entity\GameSide;
use EntityBundle\Entity\Helpers\StructureFilesHelper;
use EntityBundle\Entity\Models\Files;
use EntityBundle\Entity\Models\FilesImages;
use EntityBundle\Entity\Models\ServerModel;
use Symfony\Component\HttpFoundation\Request;

class GamesController extends BaseAdminController
{

    /**
     * Lists all game entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $games = $em->getRepository('EntityBundle:Games')->findBy(['parent'=>0, 'deleted'=>0]);

        return $this->render('AdminBundle:Games:index.html.twig', array(
            'games' => $games,
        ));
    }

    /**
     * Creates a new game entity.
     *
     */
    public function addAction(Request $request)
    {
        $game = new Games();

        if (isset($_POST['editgame'])) {
            if(isset($_POST['name']))               $game->name = $_POST['name'];
            if(isset($_POST['href']))               $game->href = $_POST['href'];
            if(isset($_POST['name_of_coin']))       $game->nameOfCoin = $_POST['name_of_coin'];
            if(isset($_POST['priority']))           $game->priority = (int)$_POST['priority'];
            if(isset($_POST['parent']))             $game->parent = (int)$_POST['parent'];
            if(isset($_POST['hasChildren']))        $game->hasChildren = (int)$_POST['hasChildren'];
            if(isset($_POST['active']))             $game->active = (int)$_POST['active'];

            $game->deleted = 0;
            $this->filesUpload($game);

            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();

            $this->addChildren($game);
            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_games_edit', array('id' => $game->id));
        }

        return $this->render('AdminBundle:Games:edit.html.twig', array(
            'game' => $game,
            'isNew' => 1,
        ));
    }

    /**
     * Displays a form to edit an existing game entity.
     *
     */
    public function editAction(Request $request, Games $game)
    {

        if (isset($_POST['editgame'])) {

            $this->addChildren($game);
            $this->addSides($game);

            if(isset($_POST['name']) && !empty($_POST['name']))                 $game->name = $_POST['name'];
            if(isset($_POST['href']) && !empty($_POST['href']))                 $game->href = $_POST['href'];
            if(isset($_POST['name_of_coin']) && !empty($_POST['name_of_coin'])) $game->nameOfCoin = $_POST['name_of_coin'];
            if(isset($_POST['priority']) && !empty($_POST['priority']))         $game->priority = (int)$_POST['priority'];
            if(isset($_POST['parent']) && !empty($_POST['parent']))             $game->parent = (int)$_POST['parent'];
            (isset($_POST['hasChildren']) && !empty($_POST['hasChildren'])) ?   $game->hasChildren = (int)$_POST['hasChildren'] : $game->hasChildren = 0;
            (isset($_POST['active']) && !empty($_POST['active'])) ?             $game->active = (int)$_POST['active'] : $game->active = 0;

            $this->filesUpload($game);

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Успех');
            return $this->redirectToRoute('admin_games_edit', array('id' => $game->id));
        }

        $gameParent = $this->getDoctrine()->getManager()->getRepository('EntityBundle:Games')->find($game->parent);
        $childrenGames = $this->getDoctrine()->getManager()->getRepository('EntityBundle:Games')->findBy(['parent'=>$game->id, 'active'=>1]);

        $em = $this->getDoctrine()->getManager();
        $servers = $em->getRepository('EntityBundle:Servers')->findBy(['gameId'=>$game->id, 'active'=>1]);
        $sides = $em->getRepository('EntityBundle:GameSide')->findBy(['gameId'=>$game->id]);



        return $this->render('AdminBundle:Games:edit.html.twig', array(
            'game' => $game,
            'gameParent' => $gameParent,
            'childrenGames' => $childrenGames,
            'servers' => $servers,
            'sides' => $sides,
        ));
    }

    /**
     * Disable a game entity.
     *
     */
    public function onofAction(Request $request, Games $game)
    {
        $em = $this->getDoctrine()->getManager();
        if($game->active == 0 ){
            $game->active = 1;
        } else {
            $game->active = 0;
        }
        $em->flush();

        return $this->redirectToRoute('admin_games');
    }

    public function deleteAction(Request $request, Games $game)
    {
        $em = $this->getDoctrine()->getManager();
        if($game->deleted == 0 ){
            $game->deleted = 1;
        } else {
            $game->deleted = 0;
        }
        $em->flush();

        return $this->onofAction($request, $game);
    }

    /**
     * Disable a game entity.
     *
     */
    public function delimgAction($id, $imgtype)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em->getRepository('EntityBundle:Games')->find($id);
        if($imgtype == 'logo'){
            @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/'.$game->logo);
            $game->logo = '';
        } else {
            @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/games/'.$game->biglogo);
            $game->biglogo = '';
        }
        $em->flush();

        return $this->redirectToRoute('admin_games_edit', ['id'=>$game->id]);
    }

    public function filesUpload(Games $game)
    {

        if(!empty($_FILES['logo']['name'])){
            $_FILES['logo']['name'] = Files::transliterate($_FILES['logo']['name']);

            $file = new FilesImages($_FILES['logo']);
            $file->upload($_FILES['logo']);
            if ($file->uploaded) {
//                    $handle->file_new_name_body   = 'image_resized';
//                    $handle->image_resize         = true;
//                    $handle->image_x              = 100;
//                    $handle->image_ratio_y        = true;
                $file->process(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/');
                if ($file->processed) {
                    @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/widget/gamesList/'.$game->logo);
                    $game->logo = $file->file_dst_name;
                    $file->clean();
                } else {
                    echo 'error : ' . $file->error;
                }
            }
        }


        //=====================================================================================
        if(!empty($_FILES['biglogo']['name'])){
            $_FILES['biglogo']['name'] = Files::transliterate($_FILES['biglogo']['name']);

            $file = new FilesImages($_FILES['biglogo']);
            $file->upload($_FILES['biglogo']);
            if ($file->uploaded) {
                $file->process(StructureFilesHelper::getFileUploadDirectory().'/image/games/');
                if ($file->processed) {
                    @unlink(StructureFilesHelper::getFileUploadDirectory().'/image/games/'.$game->biglogo);
                    $game->biglogo = $file->file_dst_name;
                    $file->clean();
                } else {
                    echo 'error : ' . $file->error;
                }
            }
        }
    }

    public function updateListAction()
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($_POST['priority'] as $gameId => $priority){
            $game = $em->getRepository('EntityBundle:Games')->find($gameId);
            $game->priority = $priority;
            $em->flush();
        }

        return $this->redirectToRoute('admin_games');
    }

    protected function addChildren(Games $gameParent){

        if(!isset($_POST['children'])){
            return;
        }

        $em = $this->getDoctrine()->getManager();
            foreach ( current($_POST['children']) as $gameName){
                if(empty($gameName)){
                    continue;
                }

                $game = new Games();
                $game->active = 0;
                $game->name = $gameName;
                $game->parent = $gameParent->id;
                $em->persist($game);
                $em->flush();
            }
        unset($em);
    }

    protected function addSides(Games $gameParent){

        if(!isset($_POST['side'])){
            return;
        }

        $em = $this->getDoctrine()->getManager();
            foreach ( current($_POST['side']) as $sideName){
                if(empty($sideName)){
                    continue;
                }

                $gameSide = new GameSide();
                $gameSide->name = $sideName;
                $gameSide->gameId = $gameParent->id;
                $em->persist($gameSide);
                $em->flush();
            }
        unset($em);
    }
}
