<?php

namespace AdminBundle\Extensions;


use EntityBundle\Entity\Models\CurrencyModel;
use Twig_Extension;
use Twig_Extension_GlobalsInterface;

class Globals extends Twig_Extension implements Twig_Extension_GlobalsInterface {

    public function getGlobals() {
        $data = array();

        $data['courses'] = CurrencyModel::getCourses();

        return $data;
    }

}