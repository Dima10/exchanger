<?php

namespace EntityBundle\Command;

use EntityBundle\Entity\Models\DB;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppCurrencyCourseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:currency_course')
            ->setDescription('Обновление курсов валют')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        if ($input->getOption('option')) {

        }
        $file = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp?date_req=".date("d/m/Y"));

        $xml = $file->xpath("//Valute[@ID='R01235']");
        $valute_usd = strval($xml[0]->Value/$xml[0]->Nominal);

        $xml = $file->xpath("//Valute[@ID='R01239']");
        $valute_eur = strval($xml[0]->Value/$xml[0]->Nominal);

        $xml = $file->xpath("//Valute[@ID='R01720']");
        $valute_uah = strval($xml[0]->Value/$xml[0]->Nominal);

        DB::getConnection()->update('currency_course',['course'=>round(str_replace(',','.',$valute_uah),2)],['id'=>2]);
        DB::getConnection()->update('currency_course',['course'=>round(str_replace(',','.',$valute_eur),2)],['id'=>3]);
        DB::getConnection()->update('currency_course',['course'=>round(str_replace(',','.',$valute_usd),2)],['id'=>4]);


        $output->writeln('');
    }

}
