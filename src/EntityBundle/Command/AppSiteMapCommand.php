<?php

namespace EntityBundle\Command;

use EntityBundle\Entity\Helpers\StructureFilesHelper;
use EntityBundle\Entity\Models\DB;
use EntityBundle\Entity\Models\GameModel;
use EntityBundle\Entity\Models\NewsModel;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppSiteMapCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:createSiteMap')
            ->setDescription('Обновление siteMap')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $simpleXML = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

        $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/');

        $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/suppliers');
        $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/reviews');
        $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/discounts');
        $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/payment');
        $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/contacts');

        $gamesData = GameModel::getActiveGamesList();

        $gamesList = [];
        foreach ($gamesData as $game){
            if($game['parent'] == 0){
                $gamesList[$game['id']] =   [
                    'id'=>$game['id'],
                    'name'=>$game['name'],
                    'logo'=>$game['logo'],
                    'bigLogo'=>$game['biglogo'],
                    'href'=>$game['href'],
                    'internalCategories'=>[

                    ]
                ];
            }
        }

        foreach ($gamesData as $game){
            if($game['parent'] != 0 && isset($gamesData[$game['parent']]) && $gamesData[$game['parent']]['active'] == 1){
                $gamesList[$game['parent']]['internalCategories'][] = [
                    'name'=>$game['name'],
                    'logo'=>$game['logo'],
                    'bigLogo'=>$game['biglogo'],
                    'href'=>$game['href'],
                ];
            }
        }

        unset($gamesList[0]);

        foreach ($gamesList as $gameMain){
            if(!empty($gameMain['internalCategories'])){
                foreach ($gameMain['internalCategories'] as $internalCategory){
                    $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/game/'.$internalCategory['href']);
                }
            } else {
                $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/game/'.$gameMain['href']);
            }
        }

        $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/news');

        $newsList = NewsModel::getActiveNewsList();
        foreach ($newsList as $new){
            $simpleXML->addChild('url')->addChild('loc', 'http://gold-buy.ru/news/'.$new['href']);
        }

        $simpleXML->saveXML(StructureFilesHelper::getCliFileUploadDirectory().'/sitemap/sitemap.xml');
    }

}
