<?php
namespace EntityBundle\Entity;

class Constants{

    CONST PROJECT_NAME = 'Gold-Buy.ru';
    CONST PROJECT_ORDER_MAIL = 'info@gold-buy.ru';
    CONST PROJECT_PROTOCOL_ST = '://';
    CONST PROJECT_PROTOCOL = 'http';
    CONST PROJECT_PROTOCOL_FULL = 'http://';


    public static $adminOrderEmails = [
        'testMail@ua.fm',
        'adminMail@ua.fm',
    ];



}