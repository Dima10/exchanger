<?php

namespace EntityBundle\Entity\Backup;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders", indexes={@ORM\Index(name="hash", columns={"hash"})})
 * @ORM\Entity
 */
class Orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=255, nullable=true)
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="main_game_id", type="integer", nullable=false)
     */
    private $mainGameId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="game_id", type="integer", nullable=false)
     */
    private $gameId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="server_id", type="integer", nullable=false)
     */
    private $serverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="side_id", type="integer", nullable=false)
     */
    private $sideId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="integer", nullable=false)
     */
    private $paymentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="currency_id", type="integer", nullable=false)
     */
    private $currencyId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="payment_amount", type="decimal", precision=12, scale=4, nullable=false)
     */
    private $paymentAmount = '0.0000';

    /**
     * @var string
     *
     * @ORM\Column(name="purchased_amount", type="decimal", precision=12, scale=4, nullable=false)
     */
    private $purchasedAmount = '0.0000';

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer", nullable=false)
     */
    private $date = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=45, nullable=true)
     */
    private $hash;


}

