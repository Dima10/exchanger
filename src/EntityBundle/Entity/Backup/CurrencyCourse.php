<?php

namespace EntityBundle\Entity\Backup;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrencyCourse
 *
 * @ORM\Table(name="currency_course")
 * @ORM\Entity
 */
class CurrencyCourse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_label", type="string", length=45, nullable=false)
     */
    private $currencyLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="course", type="decimal", precision=8, scale=3, nullable=false)
     */
    private $course = '0.000';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;


}

