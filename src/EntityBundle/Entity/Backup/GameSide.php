<?php

namespace EntityBundle\Entity\Backup;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameSide
 *
 * @ORM\Table(name="game_side")
 * @ORM\Entity
 */
class GameSide
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;


}

