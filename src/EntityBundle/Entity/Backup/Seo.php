<?php

namespace EntityBundle\Entity\Backup;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seo
 *
 * @ORM\Table(name="seo", indexes={@ORM\Index(name="url_i", columns={"url"})})
 * @ORM\Entity
 */
class Seo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=500, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_h1", type="string", length=500, nullable=true)
     */
    private $seoH1;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_title", type="string", length=500, nullable=true)
     */
    private $seoTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_keywords", type="string", length=500, nullable=true)
     */
    private $seoKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_description", type="string", length=1000, nullable=true)
     */
    private $seoDescription;


}

