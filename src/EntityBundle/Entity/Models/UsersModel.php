<?php
namespace EntityBundle\Entity\Models;


use EntityBundle\Entity\Users;

class UsersModel
{
    const ADMIN_ROLE = 'admin';
    const USER_ROLE = 'user';

    public static function getUser()
    {
        $result = isset($_COOKIE['user']) ? unserialize($_COOKIE['user']) : null;

        return $result;
    }

    public static function setUser(Users $user)
    {
        setcookie ("user", false, -5);
        setcookie ("userName", false, -5);

        $time = time()+(3600*24*60);

        setcookie ("user", serialize($user), $time);
        setcookie ("userName", $user->name, $time);

    }
}