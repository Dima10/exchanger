<?php
namespace EntityBundle\Entity\Models;


use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\TimeHelper;

class NewsModel
{

    public static $imagesDir = '/image/news/';

    public static function getActiveNewsList($limit=null){

        $limitCriteria = ($limit == null) ? '' : ' limit '.$limit;
        $sql = 'Select * from news where active=1 order by created_date desc'.$limitCriteria;
        $memcache = Memcache::getMemcache();
        if($memcache !== false){
            $result = $memcache->get(self::class,'-getActiveNewsList'.(int)$limit);
            if($result != null){
                return $result;
            } else {
                $result = DB::getConnection()->fetchAll($sql);
                $memcache->set(self::class,'-getActiveNewsList'.(int)$limit,$result, TimeHelper::THREE_HOURS);
            }
        } else {
            $result = DB::getConnection()->fetchAll($sql);
        }

        return $result;
    }
}