<?php
/**
 * Created by PhpStorm.
 * User: alastar
 * Date: 23.12.18
 * Time: 2:23
 */

namespace EntityBundle\Entity\Models;


class TelegramModel
{
    CONST EXCHANGER_GROUP_CHAT_ID = '-312309543';

    public $botId;
    public $chatId;

    function __construct($chatId, $botId = '648290607:AAH1wDKLoi3MRuzffLJGYR3OvQrw4O2btss')
    {
        $this->botId = $botId;
        $this->chatId = $chatId;
    }

    public function sendMessage($message)
    {
        file_get_contents("https://api.telegram.org/bot".$this->botId."/sendMessage?chat_id=".$this->chatId."&text=".urlencode($message));

        //        $ch=curl_init();
//        curl_setopt($ch, CURLOPT_URL,
//            'https://api.telegram.org/bot'.$this->botId.'/sendMessage');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_HEADER, false);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS,
//            'chat_id='.$this->chatId.'&text='.urlencode('ololo'));
//        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    }
}