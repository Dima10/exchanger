<?php
namespace EntityBundle\Entity\Models;


class ServerModel
{
    public static function getServersList(){
        return DB::getConnection()->fetchAll('Select s.*, g.name as gameName from servers s left JOIN games g ON(s.game_id=g.id)  where s.deleted=0 and s.active=1 order by g.name, s.name');
    }
}