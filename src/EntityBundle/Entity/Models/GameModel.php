<?php
namespace EntityBundle\Entity\Models;


use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\TimeHelper;

class GameModel
{
    public static function getActiveGamesList(){

        $sql = 'Select * from games where active=1 order by priority';
        $memcache = Memcache::getMemcache();
        if($memcache !== false){
            $result = $memcache->get(self::class,'-getActiveGamesList');
            if($result != null){
                return $result;
            } else {
                $result = DB::getConnection()->fetchAll($sql);
                $memcache->set(self::class,'-getActiveGamesList',$result, TimeHelper::DAY);
            }
        } else {
            $result = DB::getConnection()->fetchAll($sql);
        }

        $returningArray = [];
        foreach ($result as $game){
            $returningArray[$game['id']] = $game;
        }

        return $returningArray;
    }
}