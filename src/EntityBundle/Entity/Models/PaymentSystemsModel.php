<?php
namespace EntityBundle\Entity\Models;


class PaymentSystemsModel
{
   const LABEL_QIWI = 'QIWI';
   const LABEL_YANDEX = 'YANDEX';
   const LABEL_VISA_MC_YANDEX = 'VISA_MC_YANDEX';
   const LABEL_VISA_MC_EUR_USD = 'VISA_MC_EUR_USD';
   const LABEL_MOBILE = 'MOBILE';
   const LABEL_WEBMONEY= 'WEBMONEY';

    const INDEX_QIWI = 1;
    const INDEX_YANDEX = 2;
    const INDEX_VISA_MC_YANDEX = 3;
    const INDEX_VISA_MC_EUR_USD = 4;
    const INDEX_MOBILE = 5;
    const INDEX_WEBMONEY= 6;

   protected static $paymentSystems = [
       self::INDEX_QIWI =>[
           'name'=>'QIWI',
           'currencies'=>[
               CurrencyModel::INDEX_RUB =>CurrencyModel::LABEL_RUB
           ],
       ],
       self::INDEX_YANDEX =>[
           'name'=>'Яндекс.Деньги',
           'currencies'=>[
                CurrencyModel::INDEX_RUB => CurrencyModel::LABEL_RUB
           ],
       ],
//       self::LABEL_VISA_MC_YANDEX =>[
//           'name'=>'VISA / MC (Yandex)',
//           'currencies'=>[
//                 CurrencyModel::INDEX_RUB => CurrencyModel::LABEL_RUB
//           ],
//       ],
//       self::LABEL_VISA_MC_EUR_USD =>[
//           'name'=>'VISA / MC (EUR, USD, UAH)',
//           'currencies'=>[
//                CurrencyModel::INDEX_USD =>CurrencyModel::LABEL_USD,
//                CurrencyModel::INDEX_EUR =>CurrencyModel::LABEL_EUR,
//                CurrencyModel::INDEX_UAH =>CurrencyModel::LABEL_UAH,
//           ],
//       ],
//       self::LABEL_MOBILE =>[
//           'name'=>'Мобильные платежи',
//           'currencies'=>[
//               CurrencyModel::INDEX_RUB => CurrencyModel::LABEL_RUB
//           ],
//       ],
       self::INDEX_WEBMONEY =>[
           'name'=>'WebMoney',
           'currencies'=>[
               CurrencyModel::INDEX_RUB =>CurrencyModel::LABEL_RUB,
               CurrencyModel::INDEX_USD => CurrencyModel::LABEL_USD,
               CurrencyModel::INDEX_EUR =>CurrencyModel::LABEL_EUR,
           ],
       ],
   ];

   public static function getAllPaymentSystems(){
       return self::$paymentSystems;
   }

    public static function get($systemLabel){
       return self::$paymentSystems[$systemLabel];
    }
}