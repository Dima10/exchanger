<?php
namespace EntityBundle\Entity\Models;


class CurrencyModel
{
   const LABEL_RUB = 'RUB';
   const LABEL_UAH = 'UAH';
   const LABEL_EUR = 'EUR';
   const LABEL_USD = 'USD';

   const INDEX_RUB = 1;
   const INDEX_UAH = 2;
   const INDEX_EUR = 3;
   const INDEX_USD = 4;

   public static $currencyToIndex = [
       self::LABEL_RUB => self::INDEX_RUB,
       self::LABEL_UAH => self::INDEX_UAH,
       self::LABEL_EUR => self::INDEX_EUR,
       self::LABEL_USD => self::INDEX_USD,
   ];

    public static $indexToCurrency = [
        self::INDEX_RUB => self::LABEL_RUB,
        self::INDEX_UAH => self::LABEL_UAH,
        self::INDEX_EUR => self::LABEL_EUR,
        self::INDEX_USD => self::LABEL_USD,
    ];

    public static function getLabelByIndex($currencyIndex){
        return self::$indexToCurrency[$currencyIndex];
    }

    public static function getCourses(){
        $sql = 'Select * from currency_course where id!=1';
        $result = DB::getConnection()->fetchAll($sql);
        $courses = [];
        foreach ($result as $item) {
            $courses[$item['currency_label']] = $item['course'];
        }

        return $courses;
    }
}