<?php
namespace EntityBundle\Entity\Models;


use EntityBundle\Entity\Orders;
use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\TimeHelper;

class OrdersModel
{
    public static function getInfoOrderById($id){

        $sql = 'Select o.id, o.email, o.nickname, o.status, o.payment_id, o.currency_id, o.payment_amount as paymentAmount, 
                       o.purchased_amount as purchasedAmount, o.comment, g.name as mainGameName, g1.name as gameName, s.name as serverName from orders o 
                LEFT JOIN games g ON (g.id=o.main_game_id)
                LEFT JOIN games g1 ON (g1.id=o.game_id)
                LEFT JOIN servers s ON (s.id=o.server_id)

                where o.id='.$id;
        $result = DB::getConnection()->fetchAssoc($sql);

        $result['payment'] = PaymentSystemsModel::get($result['payment_id'])['name'];
        $result['currency'] = CurrencyModel::getLabelByIndex($result['currency_id']);
        $result['status'] = Orders::$_STATUSES[$result['status']];

        return $result;
    }

    public static function getDiscountForUser($userId)
    {
        $sql = 'Select sum(o.payment_amount) as payment_amount, o.currency_id, cc.course from orders o
                left join currency_course cc ON (cc.id = o.currency_id)
                where o.user_id='.$userId.' and o.status = 1 group by o.currency_id';
        $rows = DB::getConnection()->fetchAll($sql);

        $totalAmountRub = 0;
        foreach($rows as $key => $row){
            $totalAmountRub = $totalAmountRub + ($row['payment_amount'] * $row['course']);
        }

        $discount = 0;
        if($totalAmountRub >= 5000){
            $discount = 5; // 5%
        }
        elseif ($totalAmountRub >= 3000){
            $discount = 4; // 4%
        }
        elseif ($totalAmountRub >= 2000){
            $discount = 3; // 3%
        }
        elseif ($totalAmountRub >= 1000){
            $discount = 2; // 2%
        }
        elseif ($totalAmountRub >= 500){
            $discount = 1; // 1%
        }

        return $discount;

    }
}