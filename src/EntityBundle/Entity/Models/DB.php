<?php
namespace EntityBundle\Entity\Models;

use Doctrine\DBAL\DriverManager;

class DB {

    private function __clone() {}
    private function __construct() {}
    private function __wakeup() {}

    private static $instance = null;

    public static function getConnection()
    {
        if (null === self::$instance)
        {
            $config = new \Doctrine\DBAL\Configuration();
            self::$instance = DriverManager::getConnection(DBConstants::$dbParams, $config);
        }

        self::$instance->exec("set character_set_results=utf8");
        self::$instance->exec("set collation_connection=utf8_general_ci");
        self::$instance->exec('SET NAMES utf8');

        return self::$instance;
    }
}