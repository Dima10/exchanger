<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"}), @ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})})
 * @ORM\Entity
 */
class Users
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    public $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=true)
     */
    public $password;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=45, nullable=true)
     */
    public $role;

    /**
     * @var string
     *
     * @ORM\Column(name="icq", type="string", length=45, nullable=true)
     */
    public $icq;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=45, nullable=true)
     */
    public $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="telegram", type="string", length=45, nullable=true)
     */
    public $telegram;

    /**
     * @var string
     *
     * @ORM\Column(name="rating", type="decimal", precision=10, scale=2, nullable=false)
     */
    public $rating = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="approved", type="integer", nullable=false)
     */
    public $approved;
}

