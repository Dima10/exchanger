<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Models\DB;
use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\TimeHelper;

/**
 * CurrencyCource
 *
 * @ORM\Table(name="currency_cource")
 * @ORM\Entity
 */
class CurrencyCourse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_label", type="string", length=45, nullable=false)
     */
    public $currencyLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="course", type="decimal", precision=8, scale=3, nullable=false)
     */
    public $course = '0.000';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    public $date;

    public static function getCurrencyCourse(){

        $memcache = Memcache::getMemcache();
        if($memcache!=false) {
            $result = $memcache->get(self::class . 'getCurrencyCourse');
            if ($result == null) {
                $result = DB::getConnection()->fetchAll('Select * from currency_course');
                $memcache->set(self::class . 'getCurrencyCourse', $result, TimeHelper::DAY);
            }
        } else {
            $result = DB::getConnection()->fetchAll('Select * from currency_course');
        }

        return $result;

    }
}

