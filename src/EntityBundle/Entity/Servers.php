<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Models\DB;
use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\TimeHelper;

/**
 * Servers
 *
 * @ORM\Table(name="servers")
 * @ORM\Entity
 */
class Servers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="game_id", type="integer", nullable=false)
     */
    public $gameId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price_for_coin", type="decimal", precision=15, scale=10, nullable=false)
     */
    public $priceForCoin = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    public $active = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="measure_unit", type="string", length=45, nullable=true)
     */
    public $measureUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    public $deleted;

    public static function getServersByGameId($gameId){

        $memcache = Memcache::getMemcache();
        if($memcache!=false){
            $result = $memcache->get(self::class.'getServersByGameId'.$gameId);
            if($result == null){
                $result = DB::getConnection()->fetchAll('Select * from servers where game_id = "'.$gameId.'" AND active=1');
                $memcache->set(self::class.'getServersByGameId'.$gameId,$result, TimeHelper::DAY);
            }
        } else {
            $result = DB::getConnection()->fetchAll('Select * from servers where game_id = "'.$gameId.'" AND active=1');
        }

        return $result;

    }
}

