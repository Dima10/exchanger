<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seo
 *
 * @ORM\Table(name="seo", indexes={@ORM\Index(name="url_i", columns={"url"})})
 * @ORM\Entity
 */
class Seo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=500, nullable=true)
     */
    public $url;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_h1", type="string", length=500, nullable=true)
     */
    public $seoH1;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_title", type="string", length=500, nullable=true)
     */
    public $seoTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_keywords", type="string", length=500, nullable=true)
     */
    public $seoKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_description", type="string", length=1000, nullable=true)
     */
    public $seoDescription;


}

