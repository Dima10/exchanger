<?php

namespace EntityBundle\Entity\Helpers;

class StructureFilesHelper
{
    public static function getFileUploadDirectory(){
       return (strpos($_SERVER['DOCUMENT_ROOT'],'web') != false) ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT'].'/web';
    }

    public static function getCliFileUploadDirectory(){
        return str_replace('/src/EntityBundle/Entity/Helpers', '', __DIR__).'/web';
    }
}