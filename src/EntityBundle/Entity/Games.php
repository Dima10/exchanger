<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Models\DB;
use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\TimeHelper;

/**
 * Games
 *
 * @ORM\Table(name="games")
 * @ORM\Entity
 */
class Games
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=1000, nullable=true)
     */
    public $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="biglogo", type="string", length=1000, nullable=true)
     */
    public $biglogo;

    /**
     * @var string
     *
     * @ORM\Column(name="href", type="string", length=1000, nullable=true)
     */
    public $href;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    public $priority = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    public $level;

    /**
     * @var integer
     * @ORM\Column(name="parent", type="integer", nullable=false)
     */
    public $parent = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="has_children", type="integer", nullable=false)
     */
    public $hasChildren = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    public $active = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="name_of_coin", type="string", length=255, nullable=true)
     */
    public $nameOfCoin;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    public $deleted;

    public static function getChildrenById($gameId){

        $memcache = Memcache::getMemcache();
        if($memcache!=false){
            $result = $memcache->get(self::class.'_getChildrenById_'.$gameId);
            if($result == null){
                $result = DB::getConnection()->fetchAll('Select * from games where parent = "'.$gameId.'" and active=1');
                $memcache->set(self::class.'_getChildrenById_'.$gameId,$result, TimeHelper::DAY);
            }
        } else {
            $result = DB::getConnection()->fetchAll('Select * from games where parent = "'.$gameId.'" and active=1');
        }

        return $result;

    }

    public function isFreeShard(){
        return (bool)($this->parent != 0 && $this->hasChildren == 0);
    }

    public function getNameOfCoin(Games $game){
        if(!$game->isFreeShard()){
            return $game->nameOfCoin;
        } else {
            $result = DB::getConnection()->fetchAssoc('Select * from games where id = "'.$game->parent.'"');
            return $result['name_of_coin'];
        }
    }
}

