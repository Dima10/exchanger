<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Models\DB;

/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity
 */
class Orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=255, nullable=true)
     */
    public $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    public $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    public $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="main_game_id", type="integer", nullable=false)
     */
    public $mainGameId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="game_id", type="integer", nullable=false)
     */
    public $gameId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="server_id", type="integer", nullable=false)
     */
    public $serverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="side_id", type="integer", nullable=false)
     */
    public $sideId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="integer", nullable=false)
     */
    public $paymentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="currency_id", type="integer", nullable=false)
     */
    public $currencyId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="payment_amount", type="decimal", precision=20, scale=4, nullable=false)
     */
    public $paymentAmount = '0.0000';

    /**
     * @var string
     *
     * @ORM\Column(name="purchased_amount", type="decimal", precision=20, scale=4, nullable=false)
     */
    public $purchasedAmount = '0.0000';

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    public $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer", nullable=false)
     */
    public $date = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    public $status = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=45, nullable=true)
     */
    public $hash;

    public static $_STATUSES = [
        0=>'в Работе',
        1=>'Выполнен',
        2=>'Отклонен',
    ];

    public static function getOrdersList($limit=null, $userId = null){

        $where = '';
        if($userId != null){
            $where = ' o.user_id='.$userId;
        }
        if($where != ''){
            $where = ' where '.$where;
        }

        $whereLimit = '';
        if($limit != null){
            $whereLimit = ' limit '.$limit;
        }
        return DB::getConnection()->fetchAll('Select o.*, g.name as gameName, g.name_of_coin as nameOfCoin, g1.name as projectName, s.name as serverName, s.measure_unit as measureUnit, gs.name as sideName
                                              from orders o 
                                              left JOIN games g ON(o.main_game_id=g.id) 
                                              left JOIN games g1 ON(o.game_id=g1.id) 
                                              left JOIN servers s ON(o.server_id=s.id) 
                                              left JOIN game_side gs ON(o.side_id=gs.id)
                                               '.$where.'
                                              order by o.id desc '.$whereLimit);
    }

    public static function getOrderById($id){
        return DB::getConnection()->fetchAssoc('Select o.*, g.name as gameName, g.name_of_coin as nameOfCoin, g1.name as projectName, s.name as serverName, s.measure_unit as measureUnit
                                              from orders o 
                                              left JOIN games g ON(o.main_game_id=g.id) 
                                              left JOIN games g1 ON(o.game_id=g1.id) 
                                              left JOIN servers s ON(o.server_id=s.id) 
                                              where o.id = "'.$id.'"');
    }
}

