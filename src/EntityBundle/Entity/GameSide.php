<?php

namespace EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EntityBundle\Entity\Models\DB;
use ExchangerBundle\CustomModels\Memcache;
use ExchangerBundle\CustomModels\TimeHelper;

/**
 * GameSide
 *
 * @ORM\Table(name="game_side")
 * @ORM\Entity
 */
class GameSide
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    public $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    public $gameId;


    public static function getSideByGameId($gameId){

        $memcache = Memcache::getMemcache();
        if($memcache!=false) {
            $result = $memcache->get(self::class . '_getSideByGameId_' . $gameId);
            if ($result == null) {
                $result = DB::getConnection()->fetchAll('Select * from game_side where game_id = "' . $gameId . '"');
                $memcache->set(self::class . '_getSideByGameId_' . $gameId, $result, TimeHelper::DAY);
            }
        } else {
            $result = DB::getConnection()->fetchAll('Select * from game_side where game_id = "' . $gameId . '"');
        }

        return $result;

    }

}

