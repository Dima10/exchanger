

var table = document.getElementById('example2-tab2-dt');

var editingTd;

table.onclick = function(event) {

    var target = event.target;

    while (target != table) {
        if (target.className == 'edit-cancel') {
            finishTdEdit(editingTd.elem, false);
            return;
        }

        if (target.className == 'edit-ok') {
            finishTdEdit(editingTd.elem, true);
            return;
        }

        if (target.classList == 'td_edit_select_server') {
            if (editingTd) return; // already editing
            choiseServer(target);
            return;
        }

        if (target.classList == 'td_edit_select_side') {
            if (editingTd) return; // already editing
            choiseSide(target);
            return;
        }

        if (target.classList == 'td_edit_quantity') {
            if (editingTd) return; // already editing
            choiseQuantity(target);
            return;
        }

        if (target.classList == 'td_edit_price') {
            if (editingTd) return; // already editing
            choisePrice(target);
            return;
        }

        target = target.parentNode;
    }
}

function choiseServer(td) {
    editingTd = {
        elem: td,
        data: td.innerHTML
    };

    td.classList.add('td_edit_select_server'); // td, not textarea! the rest of rules will cascade


    var select = document.createElement('select');
    var newOption1 = new Option("x50");
    var newOption2 = new Option("x5");
    select.appendChild(newOption1);
    select.appendChild(newOption2);

    select.style.width = td.clientWidth = '100%';
    select.style.height = td.clientHeight = '25px';
    select.className = 'edit-area';

    select.value = td.innerHTML;
    td.innerHTML = '';
    td.appendChild(select);
    select.focus();

    td.insertAdjacentHTML("beforeEnd",
        '<div><button class="edit-ok" style="width: 50%;"><i class="fal fa-check"></i></button><button class="edit-cancel" style="width: 50%;"><i class="fal fa-times"></i></button></div>'
    );
}

function finishTdEdit(td, isOk) {
    if (isOk) {
        td.innerHTML = td.firstChild.value;
    } else {
        td.innerHTML = editingTd.data;
    }
    td.classList.remove('edit-td'); // remove edit class
    editingTd = null;
}

function choiseSide(td) {
    editingTd = {
        elem: td,
        data: td.innerHTML
    };

    td.classList.add('td_edit_select_side'); // td, not textarea! the rest of rules will cascade


    var select = document.createElement('select');
    var newOption1 = new Option("Левая");
    var newOption2 = new Option("Правая");
    select.appendChild(newOption1);
    select.appendChild(newOption2);

    select.style.width = td.clientWidth = '100%';
    select.style.height = td.clientHeight = '25px';
    select.className = 'edit-area';

    select.value = td.innerHTML;
    td.innerHTML = '';
    td.appendChild(select);
    select.focus();

    td.insertAdjacentHTML("beforeEnd",
        '<div><button class="edit-ok" style="width: 50%;"><i class="fal fa-check" "></i></button><button class="edit-cancel" style="width: 50%;"><i class="fal fa-times"></i></button></div>'
    );
}

function finishTdEdit(td, isOk) {
    if (isOk) {
        td.innerHTML = td.firstChild.value;
    } else {
        td.innerHTML = editingTd.data;
    }
    td.classList.remove('edit-td'); // remove edit class
    editingTd = null;
}

function choiseQuantity(td) {
    editingTd = {
        elem: td,
        data: td.innerHTML
    };

    td.classList.add('td_edit_quantity'); // td, not textarea! the rest of rules will cascade


    var textarea = document.createElement('input');
    var form = document.createElement('form');



    textarea.style.width = td.clientWidth = '100%';
    textarea.style.height = td.clientHeight = '25px';
    textarea.className = 'edit-area';

    textarea.value = td.innerHTML;
    td.innerHTML = '';
    td.appendChild(textarea);
    textarea.focus();

    td.insertAdjacentHTML("beforeEnd",
        '<div><button class="edit-ok" style="width: 50%;"><i class="fal fa-check"></i></button><button class="edit-cancel" style="width: 50%;"><i class="fal fa-times"></i></button></div>'
    );
}

function finishTdEdit(td, isOk) {
    if (isOk) {
        td.innerHTML = td.firstChild.value;
    } else {
        td.innerHTML = editingTd.data;
    }
    td.classList.remove('edit-td'); // remove edit class
    editingTd = null;
}

function choisePrice(td) {
    editingTd = {
        elem: td,
        data: td.innerHTML
    };

    td.classList.add('td_edit_price'); // td, not textarea! the rest of rules will cascade


    var textarea = document.createElement('input');
    var form = document.createElement('form');



    textarea.style.width = td.clientWidth = '100%';
    textarea.style.height = td.clientHeight = '25px';
    textarea.className = 'edit-area';

    textarea.value = td.innerHTML;
    td.innerHTML = '';
    td.appendChild(textarea);
    textarea.focus();

    td.insertAdjacentHTML("beforeEnd",
        '<div><button class="edit-ok" style="width: 50%;"><i class="fal fa-check"></i></button><button class="edit-cancel" style="width: 50%;"><i class="fal fa-times"></i></button></div>'
    );
}

function finishTdEdit(td, isOk) {
    if (isOk) {
        td.innerHTML = td.firstChild.value;
    } else {
        td.innerHTML = editingTd.data;
    }
    td.classList.remove('edit-td'); // remove edit class
    editingTd = null;
}




$(document).ready( function(e)
{
    $(".chosen-select1").chosen({
        width: "100%",
        disable_search: false,
        disable_search_threshold: 10,
        enable_split_word_search: false,
        max_selected_options: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_multiple: "Выберите несколько параметров",
        placeholder_text_single: "Выберите параметр",
        search_contains: true,
        display_disabled_options: false,
        display_selected_options: false,
        max_shown_results: Infinity
    });
});

$(document).ready( function(e)
{
    $(".chosen-select2").chosen({
        width: "100%",
        disable_search: false,
        disable_search_threshold: 10,
        enable_split_word_search: false,
        max_selected_options: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_multiple: "Выберите несколько параметров",
        placeholder_text_single: "Выберите параметр",
        search_contains: true,
        display_disabled_options: false,
        display_selected_options: false,
        max_shown_results: Infinity
    });
});

$(document).ready( function(e)
{
    $(".chosen-select3").chosen({
        width: "100%",
        disable_search: true,
        disable_search_threshold: 10,
        enable_split_word_search: false,
        max_selected_options: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_multiple: "Выберите несколько параметров",
        placeholder_text_single: "Выберите параметр",
        search_contains: true,
        display_disabled_options: false,
        display_selected_options: false,
        max_shown_results: Infinity
    });
});

$(document).ready( function(e)
{
    $(".chosen-select4").chosen({
        width: "100%",
        disable_search: true,
        disable_search_threshold: 10,
        enable_split_word_search: false,
        max_selected_options: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_multiple: "Выберите несколько параметров",
        placeholder_text_single: "Выберите параметр",
        search_contains: true,
        display_disabled_options: false,
        display_selected_options: false,
        max_shown_results: Infinity
    });
});

$(document).ready( function(e)
{
    $(".chosen-select5").chosen({
        width: "100%",
        disable_search: true,
        disable_search_threshold: 10,
        enable_split_word_search: false,
        max_selected_options: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_multiple: "Выберите несколько параметров",
        placeholder_text_single: "Выберите параметр",
        search_contains: true,
        display_disabled_options: false,
        display_selected_options: false,
        max_shown_results: Infinity
    });
});

$(document).ready( function(e)
{
    $(".chosen-select6").chosen({
        width: "100%",
        disable_search: true,
        disable_search_threshold: 10,
        enable_split_word_search: false,
        max_selected_options: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_multiple: "Выберите несколько параметров",
        placeholder_text_single: "Выберите параметр",
        search_contains: true,
        display_disabled_options: false,
        display_selected_options: false,
        max_shown_results: Infinity
    });
});

$(document).ready( function(e)
{
    $(".chosen-select7").chosen({
        width: "100%",
        disable_search: true,
        disable_search_threshold: 10,
        enable_split_word_search: false,
        max_selected_options: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_multiple: "Выберите несколько параметров",
        placeholder_text_single: "Выберите параметр",
        search_contains: true,
        display_disabled_options: false,
        display_selected_options: false,
        max_shown_results: Infinity
    });
});



$(function() {
    $("div[id*='menu-']").visible();
})();


function removeLine() {
    var x = document.getElementById('del-line');

    x.remove();



}