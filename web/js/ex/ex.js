$(function() {
    $("div[id*='menu-']").visible();
})();

function toggle(objName) {
    var obj = $(objName),
        blocks = $("div[id*='menu-']");

    if (obj.css("display") != "none") {
        obj.animate({ height: 'hide' }, 5);
    } else {
        var visibleBlocks = $("div[id*='menu-']:visible");

        if (visibleBlocks.length < 1) {
            obj.animate({ height: 'show' }, 5);
        } else {
            $(visibleBlocks).animate({ height: 'hide' }, 5, function() {
                obj.animate({ height: 'show' }, 5);
            });
        }
    }
}

function addSt() {
    var x = document.getElementById('addSt');
    if(x.classList.contains('fal')){
        x.classList.remove('fal');
        x.classList.add('fas');
        x.title = "Добавить в избранное";
    }else {
        x.classList.remove('fas');
        x.classList.add('fal');
        x.title = "Убрать из избранного";
    }
}

function removeLine() {
    var x = document.getElementById('del-line');

    x.remove();
}

function showGame() {
    var x = document.getElementById('fa-ch-game');
    var y = document.getElementById('ch-game-btn');

    if (x.style.display === 'none') {
        x.style.display = 'block';
        y.innerText = 'Убрать из избранного';
    } else {
        x.style.display = 'none';
        y.innerText = 'Добавить в избранное';
    }
}

function showProject() {
    var x = document.getElementById('fa-ch-project');
    var y = document.getElementById('ch-project-btn');
    if (x.style.display === 'none') {
        x.style.display = 'block';
        y.innerText = 'Убрать из избранного';
    } else {
        x.style.display = 'none';
        y.innerText = 'Добавить в избранное';
    }
}

function showServer() {
    var x = document.getElementById('fa-ch-server');
    var y = document.getElementById('ch-server-btn');
    if (x.style.display === 'none') {
        x.style.display = 'block';
        y.innerText = 'Убрать из избранного';
    } else {
        x.style.display = 'none';
        y.innerText = 'Добавить в избранное';
    }
}


function replaceIcon(){
    var x = document.getElementById('replace-icon');
    var y = document.getElementById('del-line');
    var z = document.getElementById('edit-data');
    var b = document.getElementById('remove-line');
    if(x.classList.contains('fa-eye')){
        x.classList.remove('fa-eye');
        x.classList.add('fa-eye-slash');
        y.classList.add('in-active');
        x.title = "Активировать";
        z.style.pointerEvents = 'none';
        b.style.pointerEvents = 'none';
    }else {
        x.classList.remove('fa-eye-slash');
        x.classList.add('fa-eye');
        y.classList.remove('in-active');
        x.title = "Деактивировать";
        z.style.pointerEvents = 'auto';
        b.style.pointerEvents = 'auto';
    }
}

function editData(){

    var x = document.getElementById('edit-data');
    var y = document.getElementById('td_edit_select_server');
    var z = document.getElementById('td_edit_select_side');
    var b = document.getElementById('td_edit_quantity');
    var d = document.getElementById('td_edit_price');
    var g = document.getElementById('alert-edit-sell');
    var h = document.getElementById('replace-icon');
    var j = document.getElementById('remove-line');
    var o = document.getElementById('btn_sell');

    if(x.classList.contains('fa-pencil'))
    {
        x.classList.remove('fa-pencil');
        x.classList.add('fa-edit');
        y.classList.add('td_edit_select_server');
        z.classList.add('td_edit_select_side');
        b.classList.add('td_edit_quantity');
        d.classList.add('td_edit_price');
        g.style.display = 'block';
        x.title = "Режим редактирования";
        h.style.pointerEvents = 'none';
        j.style.pointerEvents = 'none';
        o.style.display = 'none';
    }
    else {
        x.classList.remove('fa-pencil');
        x.classList.add('fa-pencil');
        x.classList.add('fa-edit');
        y.classList.remove('td_edit_select_server');
        z.classList.remove('td_edit_select_side');
        b.classList.remove('td_edit_quantity');
        d.classList.remove('td_edit_price');
        g.style.display = 'none';
        x.title = "Редактировать";
        h.style.pointerEvents = 'auto';
        j.style.pointerEvents = 'auto';
        o.style.pointerEvents = 'auto';
        o.style.display = 'block';


    }
}





