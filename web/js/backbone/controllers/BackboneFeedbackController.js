/**
 * Created by dima on 13.04.17.
 */
var BackboneFeedbackController = Marionette.View.extend({
    el: '#content',
    behaviors:  [],
    events: {
        'keyup #name': 'validateName',
        // 'keyup #email': 'validateEmail',
        'keyup #text': 'validateText',
        'click #submit': 'validateForm',
    },

    initialize: function () {
        var self = this;

    },

    validateEmail: function () {
        if(!validator.isEmail(this.$el.find('#email').val())){
            this.$el.find('#ValidationEmail').removeClass('hidden');
            return false;
        } else {
            this.$el.find('#ValidationEmail').addClass('hidden');
            return true;
        }
    },

    validateName: function () {
        if(validator.isEmpty(this.$el.find('#name').val())){
            this.$el.find('#name').addClass('formElementError');
            return false;
        } else {
            this.$el.find('#name').removeClass('formElementError');
            return true;
        }
    },

    validateText: function () {
        if(validator.isEmpty(this.$el.find('#text').val())){
            this.$el.find('#text').addClass('formElementError');
            return false;
        } else {
            this.$el.find('#text').removeClass('formElementError');
            return true;
        }
    },

    validateForm: function (e) {
        var self = this;
        var submit = true;

        if(!self.validateEmail()){
            submit = false;
        }

        if(!self.validateName()){
            submit = false;
        }

        if(!self.validateText()){
            submit = false;
        }

        if(submit == false){
            this.$el.find('#ValidationNameEmailText').removeClass('hidden');
            e.stopPropagation();
            e.preventDefault();
        }
    },

});

var feedbackController = new BackboneFeedbackController();