/**
 * Created by dima on 13.04.17.
 */
var BackboneLoginController = Marionette.View.extend({
    el: 'body',
    behaviors:  [],
    events: {
        'click #loginTopLink' : 'loginTopLinkClick',
        'click #registerTopLink' : 'registerTopLinkClick',
        'click #btn-close-login-form' : 'hideLoginModal',
        'click #btn-close-login-form-error' : 'hideLoginModalErr',
        //Registration
        // 'keyup #emailRegister': 'validateEmail',
        // 'keyup #nicknameRegister': 'validateNickname',
        // 'keyup #passwordRegister': 'validatePassword',
        'click #submitRegister': 'validateRegisterForm',
        'click #submitRecovery': 'validateRecoveryForm',
        'click #submitLogin': 'validateLoginForm',
    },

    initialize: function () {
        var self = this;

        $(function() {
            $('body').click(function (e) {
                if ($('#backDrop').length>0 && $('#backDrop').is(':visible') && $(e.target).is('#modal-login')){
                    $('#modal-login').hide();
                    $('#backDrop').hide();
                }
            });

            $('#btnNavbarMobile').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                $('#navbar').toggleClass('in');
            });

            $('#h5footerAboutf').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                $('#aboutf').toggleClass('in');
            });

            $('#h5footerAccount').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                $('#account').toggleClass('in');
            });

            $('#h5footerContact').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                $('#contact').toggleClass('in');
            });
        });

    },

    loginTopLinkClick: function (e) {
        var self = this;
        self.showLoginModal();
        self.resetActiveLiSectionLogin();
        self.resetActiveTabPane();
        $('#sectionLiLogin').addClass('active');
        $('#Section1').addClass('in').addClass('active');

        e.preventDefault();
        e.stopPropagation();
    },

    registerTopLinkClick: function (e) {
        var self = this;
        self.showLoginModal();
        self.resetActiveLiSectionLogin();
        self.resetActiveTabPane();
        $('#sectionLiRegister').addClass('active');
        $('#Section2').addClass('in').addClass('active');

        e.preventDefault();
        e.stopPropagation();
    },

    showLoginModal: function(){
        $('#modal-login').show();
        $('#backDrop').show();
    },

    hideLoginModal: function(){
        $('#modal-login').hide();
        $('#backDrop').hide();
    },



    hideLoginModalErr: function(){
        $('#loginErrorDiv').addClass('hidden');
    },

    resetActiveLiSectionLogin: function(){
        $('.sectionLogin').removeClass('active');
    },

    resetActiveTabPane: function(){
        $('.tab-pane').removeClass('in').removeClass('active');
    },

    validateEmail: function (id) {
        if(!validator.isEmail(this.$el.find('#'+id).val())){
            this.$el.find('#'+id+'Validation').removeClass('hidden');
            return false;
        } else {
            this.$el.find('#'+id+'Validation').addClass('hidden');
            return true;
        }
    },

    validateNickname: function (id) {
        if(validator.isEmpty(this.$el.find('#'+id).val())){
            this.$el.find('#'+id+'Validation').removeClass('hidden');
            return false;
        } else {
            this.$el.find('#'+id+'Validation').addClass('hidden');
            return true;
        }
    },

    validateRegisterPassword: function (id, idConfirm) {

        if(
            validator.isEmpty(this.$el.find('#'+id).val()) ||
            validator.isEmpty(this.$el.find('#'+idConfirm).val()) ||
            this.$el.find('#'+id).val().length < 5 ||
            this.$el.find('#'+id).val() != this.$el.find('#'+idConfirm).val()
        ){
            this.$el.find('#'+id+'Validation').removeClass('hidden');
            return false;
        } else {
            this.$el.find('#'+id+'Validation').addClass('hidden');
            return true;
        }

    },

    validateRegisterForm: function (e) {
        var self = this;
        var submit = true;

        if(!self.validateEmail('emailRegister')){
            submit = false;
        }

        if(!self.validateNickname('nicknameRegister')){
            submit = false;
        }

        if(!self.validateRegisterPassword('passwordRegister', 'confirmPasswordRegister')){
            submit = false;
        }

        if(submit == false){
            e.stopPropagation();
            e.preventDefault();
            return;
        }

        $.ajax({
            type: "POST",
            url: "/register",
            data: {
                'emailRegister': $('#emailRegister').val(),
                'nicknameRegister': $('#nicknameRegister').val(),
                'passwordRegister': $('#passwordRegister').val()
            }, // Send dataFields var
            success: function(data){ // If success
                $('.registerInfo').addClass('hidden');
                var dataObj = JSON.parse(data);
                if(typeof dataObj.error != 'undefined'){
                    $('#registerErrorDiv').removeClass('hidden');
                } else {
                    $('#registerSuccessDiv').removeClass('hidden');
                }
            },
            error: function() {
            }
        });

        e.stopPropagation();
        e.preventDefault();
    },

    validateRecoveryForm: function (e) {
        var self = this;
        var submit = true;

        if(!self.validateEmail('emailRecovery')){
            submit = false;
        }

        if(submit == false){
            e.stopPropagation();
            e.preventDefault();
            return;
        }

        $.ajax({
            type: "POST",
            url: "/recoverypass",
            data: {
                'emailRecovery': $('#emailRecovery').val(),
            },
            success: function(data){ // If success
                $('.sectionDivInfo').addClass('hidden');
                var dataObj = JSON.parse(data);
                if(typeof dataObj.error != 'undefined'){
                    $('#recoveryErrorDiv').removeClass('hidden');
                } else {
                    $('#recoverySuccessDiv').removeClass('hidden');
                }
            },
            error: function() {
            }
        });

        e.stopPropagation();
        e.preventDefault();

    },

    validateLoginForm: function (e) {
        var self = this;
        var submit = true;

        if(!self.validateNickname('nicknameLogin')){
            submit = false;
        }

        if(!self.validateNickname('passwordLogin')){
            submit = false;
        }

        if(submit == false){
            e.stopPropagation();
            e.preventDefault();
            return;
        }

        $.ajax({
            type: "POST",
            url: "/login",
            data: {
                'nicknameLogin': $('#nicknameLogin').val(),
                'passwordLogin': $('#passwordLogin').val(),
                'remember': $('#remember').is(':checked'),
            }, // Send dataFields var
            success: function(data){ // If success
                $('.registerInfo').addClass('hidden');
                var dataObj = JSON.parse(data);
                if(typeof dataObj.error != 'undefined'){
                    $('#loginErrorDiv').removeClass('hidden');
                } else {
                    window.location.href='/cabinet'
                }
            },
            error: function() {
            }
        });

        e.stopPropagation();
        e.preventDefault();
    },

});

var loginController = new BackboneLoginController();