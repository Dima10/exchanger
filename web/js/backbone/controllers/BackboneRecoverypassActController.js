/**
 * Created by dima on 13.04.17.
 */
var BackboneRecoverypassActController = Marionette.View.extend({
    el: '#content',
    behaviors:  [],
    serverPrice: 0,
    measureUnit: '',
    currencyCourse: 0,
    events: {
        'keyup #newPass': 'validateForm',
        'click #submit': 'validateForm',
    },

    initialize: function () {
        var self = this;

    },

    validateNewPass: function () {
        if(
            validator.isEmpty(this.$el.find('#newPass').val()) ||
            this.$el.find('#newPass').val().length < 5
        ){
            this.$el.find('#newPass').addClass('formElementError');
            return false;
        } else {
            this.$el.find('#newPass').removeClass('formElementError');
            return true;
        }
    },

    validateForm: function (e) {
        var self = this;
        var submit = true;

        if(!self.validateNewPass()){
            submit = false;
        }

        if(submit == false){
            e.stopPropagation();
            e.preventDefault();
        }
    },
});

var recoverypassActController = new BackboneRecoverypassActController();