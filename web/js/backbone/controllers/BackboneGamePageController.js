/**
 * Created by dima on 13.04.17.
 */
var BackboneGamePageController = Marionette.View.extend({
    el: '#content',
    behaviors:  [],
    serverPrice: 0,
    measureUnit: '',
    currencyCourse: 0,
    events: {
        'change #projectsSelect': 'projectsSelectChange',
        'change #serverSelect': 'serverSelectChange',
        'change #paymentSelect': 'paymentSelectChange',
        'change #currencySelect': 'currencySelectChange',
        'keyup #paymentAmount': 'paymentAmountKeyUp',
        'keydown #paymentAmount': 'paymentAmountKeyDown',
        'keyup #purchasedAmount': 'purchasedAmountKeyDown',
        'keydown #purchasedAmount': 'purchasedAmountKeyUp',
        'keydown #paymentAmount, #purchasedAmount': 'validateNumeric',
        'keypress #paymentAmount, #purchasedAmount': 'validateNumeric',
        'change #paymentAmount, #purchasedAmount': 'validateNumeric',
        'keyup #email': 'validateEmail',
        'click #submit': 'validateForm',
    },

    initialize: function () {
        var self = this;

        $(function() {
            self.projectsSelectChange();
            self.serverSelectChange();
            self.paymentSelectChange();
            self.currencySelectChange();
            jQuery('#paymentAmount').mask('000 000 000 000', {
                reverse: true,
            });

            jQuery('#purchasedAmount').mask('### ### ### ### #,000', {
                reverse: true,
            });
        });

    },

    validateNumeric: function (e) {

        this.$el.find('.amount').removeClass('formElementError');

        var iKeyCode = (e.which) ? e.which : e.keyCode;
        if ((iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)))
            return false;

        return true;

    },

    validateEmail: function () {
        if(!validator.isEmail(this.$el.find('#email').val())){
            this.$el.find('#email').addClass('formElementError');
            return false;
        } else {
            this.$el.find('#email').removeClass('formElementError');
            return true;
        }
    },

    validateForm: function (e) {
        var self = this;
        var submit = true;


        if(validator.isEmpty(this.$el.find('#nickname').val())){
            this.$el.find('#nickname').addClass('formElementError');
            submit = false;
        } else {
            this.$el.find('#nickname').removeClass('formElementError');
        }

        if(!validator.isNumeric(this.$el.find('#paymentAmount').val().replace(/\s/gi, "").replace(',',',')) || !validator.isNumeric(this.$el.find('#purchasedAmount').val().replace(/\s/gi, "").replace(',','.'))){
            this.$el.find('.amount').addClass('formElementError');
            submit = false;
        }


        if(!self.validateEmail()){
            // this.$el.find('#email').addClass('formElementError');
            submit = false;
        }

        if(submit == false){
            e.stopPropagation();
            e.preventDefault();
        }

        this.$el.find('#paymentAmount').val(this.$el.find('#paymentAmount').val().replace(/\s/gi, "").replace(',','.'));
        this.$el.find('#purchasedAmount').val(this.$el.find('#purchasedAmount').val().replace(/\s/gi, "").replace(',','.'));

    },

    projectsSelectChange: function () {
        var self = this;
        $.ajax({
            type: "POST",
            url: "/ajax/getServersByProjectId", // call the php file ajax/tuto-autocomplete.php
            data: {'projectId': this.$el.find('#projectsSelect').val()}, // Send dataFields var
            async: false,
            success: function(data){ // If success
                var parsedData = JSON.parse(data);

                self.$el.find('#serverSelect').children('option').remove();
                self.$el.find('#gameId').val(self.$el.find('#projectsSelect').val());

                $.each(parsedData, function( index, value ) {
                    self.$el.find('#serverSelect').append('<option value="'+value.id+'" data-price="'+value.price_for_coin+'" data-measure_unit="'+value.measure_unit+'">'+value.name+'</option>')
                });
            },
            error: function() {
            }
        });
    },

    serverSelectChange: function () {
        var self = this;
        this.$el.find('#paymentAmount').val('');
        this.$el.find('#purchasedAmount').val('');
        self.serverPrice = $('#serverSelect option:selected').attr('data-price');
        self.measureUnit = $('#serverSelect option:selected').attr('data-measure_unit');
        self.changePurchasedAmountDiv();
    },

    paymentSelectChange: function () {
        var self = this;
        this.$el.find('#currencySelect').children('option').remove();
        $.each(window.paymentsJson[$('#paymentSelect').val()].currencies, function( index, value ) {
            self.$el.find('#currencySelect').append('<option value="'+index+'">'+value+'</option>');
        });
        self.currencySelectChange();
    },

    currencySelectChange: function () {
        this.$el.find('#currencySpan').html(this.$el.find('#currencySelect option:selected').text());
        this.currencyCourse = window.currencyCourse[this.$el.find('#currencySelect').val()];
        this.paymentAmountKeyUp();
    },

    paymentAmountKeyUp: function () {
        var pAmount = this.$el.find('#paymentAmount').val().replace(/\s/gi, "");
        var purchasedAmount = Math.round((pAmount/this.serverPrice)* parseFloat(this.currencyCourse));
        purchasedAmount = purchasedAmount * (100+parseInt(this.$el.find('#discount').val()))/100;
        this.$el.find('#purchasedAmount').val(new Intl.NumberFormat('ru-RU').format(purchasedAmount));
        this.changePurchasedAmountDiv();
    },

    purchasedAmountKeyUp: function () {
        var pAmount = this.$el.find('#purchasedAmount').val().replace(/\s/gi, "").replace(',','.');
        var paymentAmount = ((pAmount*this.serverPrice)/parseFloat(this.currencyCourse));
        paymentAmount = (paymentAmount / ((100+parseInt(this.$el.find('#discount').val()))/100)).toFixed(2);
        this.$el.find('#paymentAmount').val(new Intl.NumberFormat('ru-RU').format(paymentAmount));
        this.changePurchasedAmountDiv();
    },

    changePurchasedAmountDiv: function () {

        var divider = 1;
        if(this.measureUnit == 'k'){
            divider = 1000;
        } else if(this.measureUnit == 'kk'){
            divider = 1000000;
        } else if(this.measureUnit == 'kkk'){
            divider = 1000000000;
        }

        var pAmount = this.$el.find('#purchasedAmount').val().replace(/\s/gi, "").replace(',','.');
        this.$el.find('#purchasedAmountDiv').html((pAmount/divider).toFixed(5) +' '+ this.measureUnit);
    }
});

var gamePageController = new BackboneGamePageController();