/**
 * Created by dima on 13.04.17.
 */
var BackboneCabinetProfileController = Marionette.View.extend({
    el: '#content',
    behaviors:  [],
    events: {

        'click #submitProfile': 'validateProfileForm',
        'click #approvingEmail': 'approvingEmailClick',
    },

    initialize: function () {
        var self = this;

    },

    validateEmail: function (id) {
        if(!validator.isEmail(this.$el.find('#'+id).val())){
            this.$el.find('#'+id+'Validation').removeClass('hidden');
            return false;
        } else {
            this.$el.find('#'+id+'Validation').addClass('hidden');
            return true;
        }
    },

    validateNickname: function (id) {
        if(validator.isEmpty(this.$el.find('#'+id).val())){
            this.$el.find('#'+id+'Validation').removeClass('hidden');
            return false;
        } else {
            this.$el.find('#'+id+'Validation').addClass('hidden');
            return true;
        }
    },

    validatePassword: function (id) {

        if(
            !validator.isEmpty(this.$el.find('#'+id).val()) &&
            this.$el.find('#'+id).val().length < 5
        ){
            this.$el.find('#'+id+'Validation').removeClass('hidden');
            return false;
        } else {
            this.$el.find('#'+id+'Validation').addClass('hidden');
            return true;
        }

    },

    validateProfileForm: function (e) {
        var self = this;
        var submit = true;

        if(!self.validateEmail('email')){
            submit = false;
        }

        if(!self.validateNickname('nickname')){
            submit = false;
        }

        if(!self.validatePassword('password')){
            submit = false;
        }

        if(submit == false){
            e.stopPropagation();
            e.preventDefault();
            return;
        }

    },

    approvingEmailClick: function (e) {

        $.ajax({
            type: "POST",
            url: "/emailApprovingFromCabinet",
            data: {
                'userEmail': $('#email').val(),
            }, // Send dataFields var
            success: function(data){ // If success
                // var dataObj = JSON.parse(data);
                $('#approvingEmailFromCabinet').removeClass('hidden');
            },
            error: function() {
            }
        });

        e.stopPropagation();
        e.preventDefault();
    },

});

var cabinetProfileController = new BackboneCabinetProfileController();