var localStorageModel = {
        setData: function(key, val) {
            window.localStorage && window.localStorage.setItem(key, JSON.stringify(val));
            return this;
        },
        getData: function(key) {
            var lsData = window.localStorage.getItem(key);
            if (isJson(lsData)) {
                var parsedData = JSON.parse(lsData);
                if(parsedData !== null){
                    return parsedData;
                }
            }
            return {};
        },
        deleteData: function(key) {
            return window.localStorage && window.localStorage.removeItem(key);
        },
        getCount: function(key) {
            var dataStringify = window.localStorage.getItem(key);
            if (dataStringify === null) {
                return 0;
            } else {
                var data = JSON.parse(dataStringify);
                return Object.keys(data).length
            }
        }
    };

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
