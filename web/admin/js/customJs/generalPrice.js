$(function() {

    $('#stillData').on('click', function (e) {

        $.ajax({
            type: "POST",
            url: "/admin/ajax/getDataForGeneralPricePage", // call the php file ajax/tuto-autocomplete.php
            data: {}, // Send dataFields var
            async: false,
            success: function(data){ // If success
                var parsedResult = JSON.parse(data);
                // $('#stilled').html(parsedResult);
                $(parsedResult).each(function (index, item) {
                    var i = 0;
                    while (i < 1000) {
                        if(typeof item[i] != 'undefined'){
                            $('#serverId_'+i).val(item[i].cost);
                            $('#serverId_'+i).css('color', 'yellow');
                            $('#serverId_'+i).css('background', 'black');
                        }
                        // console.log(item[i]);
                        i++;
                    }
                });
                $('#serverId_46').css('background', 'red'); // KRONOS
                $('#serverId_4').css('background', 'red'); // Scryde x100
            },
            error: function() {
            }
        });

        e.preventDefault();
        e.stopPropagation();
    });

});