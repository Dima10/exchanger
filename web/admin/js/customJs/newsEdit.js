$(function() {

    var holder = document.getElementById('holder'),
        tests = {
            filereader: typeof FileReader != 'undefined',
            dnd: 'draggable' in document.createElement('span'),
            formdata: !!window.FormData,
            progress: "upload" in new XMLHttpRequest
        },
        support = {
            filereader: document.getElementById('filereader'),
            formdata: document.getElementById('formdata'),
            progress: document.getElementById('progress')
        },
        acceptedTypes = {
            'image/png': true,
            'image/jpeg': true,
            'image/gif': true
        },
        fileupload = document.getElementById('upload');

    "filereader formdata progress".split(' ').forEach(function (api) {
        if (tests[api] === false) {
            support[api].className = 'fail';
        } else {
            support[api].className = 'hidden';
        }
    });

    function previewfile(file) {
        if (tests.filereader === true && acceptedTypes[file.type] === true) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $('#holder').html('');
                var image = new Image();
                image.src = event.target.result;
                image.width = 250; // a fake resize
                holder.appendChild(image);

                var fileType = 'jpg';
                if(file.type.split('/').pop() != 'image/jpeg'){
                    fileType = file.type.split('/').pop();
                }

                $.ajax({
                    type: "POST",
                    url: "/admin/ajax/uploadfile", // call the php file ajax/tuto-autocomplete.php
                    data: {
                        'imageSrcBase64': image.src,
                        'imgType': fileType,
                        'savePath': '/image/news/'+location.href.split('/').pop()+'/'+file.name
                    }, // Send dataFields var
                    success: function(data){ // If success
                       var dataObj = JSON.parse(data);

                       var toPrepend = '<div class="col-sm-3 prependableImgDiv" style="">'
                                            +'<div class="widget">'
                                                +'<div class="widget-content padding">'
                                                    +'<img style="max-width: 300px; max-height: 300px" src="'+dataObj.link+'">'
                                                    +'<a style="color: #000; text-decoration: underline" href="'+dataObj.link+'">'+dataObj.link+'</a>'
                                                    +'<br />'
                                                    +'<a class="prependableDelA" style="color: red; font-size: 18px; font-weight: bold;" href="'+dataObj.delLink+'">Удалить</a>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>';

                        $('#staticImage').prepend(toPrepend);

                    },
                    error: function() {
                    }
                });
            };

            reader.readAsDataURL(file);
        }  else {
            holder.innerHTML += '<p>Uploaded ' + file.name + ' ' + (file.size ? (file.size/1024|0) + 'K' : '');
            console.log(file);
        }
    }

    function readfiles(files) {
        var formData = tests.formdata ? new FormData() : null;
        for (var i = 0; i < files.length; i++) {
            if (tests.formdata) formData.append('file', files[i]);
            previewfile(files[i]);
        }
    }

    if (tests.dnd) {
        holder.ondragover = function () { this.className = 'hover'; return false; };
        holder.ondragend = function () { this.className = ''; return false; };
        holder.ondrop = function (e) {
            this.className = '';
            e.preventDefault();
            readfiles(e.dataTransfer.files);
        }
    } else {
        fileupload.className = 'hidden';
        fileupload.querySelector('input').onchange = function () {
            readfiles(this.files);
        };
    }

});


$('#staticImage').on('click','a.prependableDelA',function (e) {

    var href = $(this).attr('href');
    $.ajax({
        type: "POST",
        url: "/admin/ajax/delimg", // call the php file ajax/tuto-autocomplete.php
        data: {'delImgSrc': href}, // Send dataFields var
        async: false,
        success: function(data){ // If success
        },
        error: function() {
        }
    });

    $(this).parents('.prependableImgDiv').remove();


    e.preventDefault();
    e.stopPropagation();
});

// $('#resizeCkEditor').on('click', function (e) {
//
//     CKEDITOR.instances.ckeditor.resize('100%', '400');
//
//     e.preventDefault();
//     e.stopPropagation();
// });
