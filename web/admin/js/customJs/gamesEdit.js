$(function() {
        $("#parentFake").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/admin/ajax/getlistgames",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        var json = JSON.parse(data);
                        response(json);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#parent").val(ui.item.id);
                $("#parentFake").val(ui.item.value);
            }
        });

        $('#addChild').on('click', function (e) {
            $('#childDiv').append('<div class="form-group"><input type="text" name="children['+e.target.dataset.gameid+'][]" class="form-control" value="" placeholder="child"></div>');
            e.preventDefault();
            e.stopPropagation();
        });

        $('#hasChildrenDiv .toggleChildrenDiv').on('click', function (e) {
            $('#childDiv').toggle();
        });


        $('#addSide').on('click', function (e) {
            $('#sidesDiv').append('<div class="form-group"><input type="text" name="side['+e.target.dataset.gameid+'][]" class="form-control" value="" placeholder="side"></div>');
            e.preventDefault();
            e.stopPropagation();
        });

        // $('#hasChildrenDiv .toggleChildrenDiv').on('click', function (e) {
        //     $('#sidesDiv').toggle();
        // });

        $('.delSide').on('click', function (e) {

            $.ajax({
                type: "POST",
                url: "/admin/ajax/delGameSide", // call the php file ajax/tuto-autocomplete.php
                data: {'sideid': e.target.dataset.sideid}, // Send dataFields var
                async: false,
                success: function(data){ // If success
                    console.log(JSON.parse(data));
                    $(e.target).parents('.sideADiv').remove();
                },
                error: function() {
                }
            });

            e.preventDefault();
            e.stopPropagation();
        });
});