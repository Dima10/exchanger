$(function() {

        $('#editfeedback').on('click', function (e) {
            $('#answerForm').toggleClass('hidden');
            e.preventDefault();
            e.stopPropagation();
        });

        $('#hasChildrenDiv .toggleChildrenDiv').on('click', function (e) {
            $('#childDiv').toggle();
        });


        $('#addSide').on('click', function (e) {
            $('#sidesDiv').append('<div class="form-group"><input type="text" name="side['+e.target.dataset.gameid+'][]" class="form-control" value="" placeholder="side"></div>');
            e.preventDefault();
            e.stopPropagation();
        });

});